import React from 'react';
import store, { persistor } from "./src/store";
import { NavigationContainer } from "@react-navigation/native";
import {Provider, useSelector} from "react-redux";
import BaseNavigator from "./src/navigation/BaseNavigator";
import {PersistGate} from "redux-persist/integration/react";

const App = () => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <NavigationContainer>
                    <BaseNavigator />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    );
}

export default App;
