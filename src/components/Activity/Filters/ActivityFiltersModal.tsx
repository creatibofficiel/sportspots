import { Modal, Text, TouchableOpacity, View } from "react-native";
import COLORS from "../../../theme/layout/colors";
import { Icon } from "react-native-elements";
import { BOTTOM, FORM_GROUP } from "../../../styles/general";
import LocationInput from "../../Form/LocationInput/LocationInput";
import FormDatePicker from "../../Form/FormDatePicker/FormDatePicker";
import { Controller, useWatch } from "react-hook-form";
import { RadioInput } from "../../Form/RadioInput/RadioInput";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { CustomButton, StyleType } from "../../Form/CustomButton/CustomButton";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../../../store/ducks/search-sports";
import useTrans from "../../../hooks/translation";

interface FiltersModalType {
    open: boolean;
    control: any;
    closeModal: any;
    handleSubmit: any;
    getValues: any;
}

export const ActivityFiltersModal = ({ open, closeModal, control, getValues, handleSubmit }: FiltersModalType ) => {

    const dispatch = useDispatch();

    const sportsData = useSelector(state => state.searchSport);

    const { t } = useTrans();

    useEffect(() => {
        dispatch(Actions.requestSearchSports());
    }, []);

    const participantsCount = useWatch({
        control,
        name: 'participantsCount',
        defaultValue: [5, 10],
    });

    const distanceRadius = useWatch({
        control,
        name: 'distanceRadius',
        defaultValue: getValues('distanceRadius') ?? 5,
    });

    const averageAge = useWatch({
        control,
        name: 'averageAge',
        defaultValue: getValues('averageAge') ?? 16,
    });

    return <Modal visible={open}>
        <View style={{ position: 'absolute', zIndex: 10, top: '5%', paddingHorizontal: 30, width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontSize: 22, fontWeight: 'bold', color: '#000B22' }}>{t('Filters')}</Text>
            <TouchableOpacity onPress={closeModal}>
                <View style={{ width: 95, height: 30, paddingHorizontal: 10, paddingVertical: 7, backgroundColor: COLORS.primary, display: 'flex', flexDirection: 'row', alignItems: 'center', borderRadius: 50, elevation: 1 }}>
                    <Icon type="material" color={'white'} name={"filter-alt"} size={12}/>
                    <Text style={{ color: 'white', marginLeft: 10, fontSize: 12 }}>{t('Map')}</Text>
                </View>
            </TouchableOpacity>
        </View>
        <View style={{ height: '100%', marginTop: 40 }}>
            <View style={FORM_GROUP}>
                <View
                    style={{
                        marginTop: 50,
                        paddingHorizontal: 25,
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Localisation de l'activité</Text>
                    <LocationInput name={'location'}
                                   control={control}
                                   rules={{ required: true}} />
                </View>
                <View style={{
                    marginTop: 50,
                    paddingHorizontal: 25
                }}>
                    <FormDatePicker
                        label={'Date de l\'activité'}
                        name={'activityDate'}
                        mode={"datetime"}
                        control={control}
                        placeholder={'Sélectionnez une date'}
                        format={"DD-MM-YYYY HH:mm"}
                        rules={{required: true}}
                    />
                </View>
                <View style={{
                    marginTop: 50,
                    paddingHorizontal: 25,
                }}>
                    <Text style={{
                        fontSize: 15,
                    }}>Choix des sports</Text>
                    <Controller
                        control={control}
                        name="searchSports"
                        render={({field}) => (
                            <RadioInput
                                {...field}
                                multiple={true}
                                max={3}
                                list={sportsData.data}
                            />
                        )}
                    />
                </View>
                <View style={{
                    marginTop: 50,
                    paddingHorizontal: 25,
                }}>
                    <Text style={{color: '#8d8d8d'}}>
                        Distance maximal depuis votre position : {distanceRadius} km
                    </Text>
                    <Controller
                        control={control}
                        name="distanceRadius"
                        defaultValue={[5]}
                        render={({field: {value, onChange}}) => {
                            return (
                                <MultiSlider
                                    min={0}
                                    values={value}
                                    selectedStyle={{backgroundColor: COLORS.primary}}
                                    max={100}
                                    onValuesChange={data => onChange(data)}
                                    enabledTwo={false}
                                />
                            );
                        }}
                    />
                    <Text style={{color: '#8d8d8d'}}>
                        Nombre min / max de personnes dans l'activité
                    </Text>
                    <View style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between"}}>
                        <Text>
                            {participantsCount[0]}
                        </Text>
                        <Text>
                            {participantsCount[1]}
                        </Text>
                    </View>
                    <Controller
                        control={control}
                        name="participantsCount"
                        defaultValue={[5, 10]}
                        render={({field: {value, onChange}}) => {
                            return (
                                <MultiSlider
                                    min={0}
                                    values={value}
                                    selectedStyle={{backgroundColor: COLORS.primary}}
                                    max={20}
                                    onValuesChange={data => onChange(data)}
                                    enabledTwo={true}
                                />
                            );
                        }}
                    />
                    <Text style={{color: '#8d8d8d'}}>
                        Moyenne d'âge de l'activité : {averageAge} ans
                    </Text>
                    <Controller
                        control={control}
                        name="averageAge"
                        defaultValue={[5]}
                        render={({field: {value, onChange}}) => {
                            return (
                                <MultiSlider
                                    min={0}
                                    values={value}
                                    selectedStyle={{backgroundColor: COLORS.primary}}
                                    max={100}
                                    onValuesChange={data => onChange(data)}
                                    enabledTwo={false}
                                />
                            );
                        }}
                    />
                </View>
            </View>
            <View style={BOTTOM}>
                <CustomButton
                    text="Rechercher"
                    onPress={handleSubmit}
                    type={StyleType.PRIMARY}
                    style={{
                        marginLeft: "15%",
                        width: "70%",
                        marginVertical: 25,
                    }}
                />
            </View>
        </View>
    </Modal>;
}
