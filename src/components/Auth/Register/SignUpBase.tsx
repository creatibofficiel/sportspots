import React, { useEffect } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import FormInput from '../../Form/FormInput/FormInput';
import {StyleType, CustomButton} from '../../Form/CustomButton/CustomButton';
import { GoogleSignin } from "@react-native-google-signin/google-signin";
import { FORM_GROUP } from "../../../styles/general";
import { useWatch } from "react-hook-form";
import useTrans from "../../../hooks/translation";

export const SignUpBase = ({ nextStep, setValue, handleSubmit, control }: any) => {

    const { t } = useTrans();

    const password = useWatch({
        control,
        name: "plainPassword",
    });
    const googleId = useWatch({
        control,
        name: "googleId",
    });
    const facebookId = useWatch({
        control,
        name: "facebookId",
    });
    const appleId = useWatch({
        control,
        name: "appleId",
    });

  useEffect(() => {
      GoogleSignin.configure({
          scopes: ['email'],
          webClientId: '308409705099-vqtfrocq8nejrteekaon2oa3831n9obq.apps.googleusercontent.com',
          offlineAccess: true,
      });
  }, []);

  const onSignIgnWithGoogle = async () => {
      try {
          await GoogleSignin.hasPlayServices();
          const { accessToken, idToken }: any = await GoogleSignin.signIn();
          console.log(accessToken, idToken);
      } catch (error) {
          console.log(error);
      }
  };

  return (
    <View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          marginBottom: 14,
          shadowColor: '#3E68B2',
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowRadius: 8.3,
          elevation: 13,
        }}>
        <CustomButton
          text="FACEBOOK"
          onPress={onSignIgnWithGoogle}
          type={StyleType.LIGHT}
          fgColor={'#3E6AB2'}
          style={{
            borderBottomRightRadius: 0,
            borderTopRightRadius: 0,
            width: '50%',
            backgroundColor: 'white',
            borderWidth: 1.5,
            borderColor: '#EBEFF6',
          }}
        />
        <CustomButton
          text="GOOGLE"
          onPress={onSignIgnWithGoogle}
          type={StyleType.LIGHT}
          fgColor={'#EC6A5E'}
          style={{
            borderBottomLeftRadius: 0,
            borderTopLeftRadius: 0,
            width: '50%',
            backgroundColor: 'white',
            borderWidth: 1.5,
            borderColor: '#EBEFF6',
            borderLeftWidth: 0,
          }}
        />
      </View>

        <View style={{ display: 'none' }}>
            <FormInput
                name="googleId"
                control={control}
                label={'GoogleId'}
                placeholder="GoogleId"
            />
        </View>

        <View style={{ display: 'none' }}>
            <FormInput
                name="facebookId"
                control={control}
                label={'FacebookId'}
                placeholder="FacebookId"
            />
        </View>

        <View style={{ display: 'none' }}>
            <FormInput
                name="appleId"
                control={control}
                label={'AppleId'}
                placeholder="AppleId"
            />
        </View>

      <FormInput
          name="email"
          control={control}
          label={t('Email')}
          placeholder="Email"
          rules={{
              required: true,
              pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                  message: t('Invalid email address')
              }
            }}
      />

      <View style={FORM_GROUP}>
          <FormInput
              name={"plainPassword"}
              control={control}
              label={t('Password')}
              placeholder={t('Password')}
              secureTextEntry
              rules={{
                  validate: (value: string) => {
                      if(!googleId && !facebookId && !appleId && value.length < 8) return 'Le mot de passe doit contenir au moins 8 lettres'
                      return true;
                  }
              }}
          />
      </View>

      <View style={FORM_GROUP}>
        <FormInput
            name={"repeatPassword"}
            control={control}
            label={t('Repeat password')}
            placeholder={t('Repeat password')}
            secureTextEntry
            rules={{
                  validate: (value: string) => {
                      if(!googleId && !facebookId && !appleId && value !== password) return t('Password are not the same');
                  }
              }}
            />
        </View>

      <View style={FORM_GROUP}>
            <CustomButton
                text={t('Next')}
                onPress={handleSubmit(nextStep, (err) => {
                    console.log(err);
                })}
                type={StyleType.PRIMARY}
                style={{
                    marginTop: 'auto',
                }}
            />
        </View>
    </View>
  );
};

export default SignUpBase;
