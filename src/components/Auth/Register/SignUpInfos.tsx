import React from 'react';
import {View} from 'react-native';
import FormInput from '../../Form/FormInput/FormInput';
import {StyleType, CustomButton} from '../../Form/CustomButton/CustomButton';
import {RadioInput} from '../../Form/RadioInput/RadioInput';
import {BOTTOM, FORM_GROUP} from '../../../styles/general';
import {Controller} from 'react-hook-form';
import FormDatePicker from '../../../components/Form/FormDatePicker/FormDatePicker';
import useTrans from "../../../hooks/translation";

const SEX_LIST = [
  {
    name: 'Homme',
    id: 'Man',
  },
  {
    name: 'Femme',
    id: 'Woman',
  },
];

export const SignUpScreen = ({control, handleSubmit, nextStep}: any) => {

  const { t } = useTrans();

  return (
    <View style={{ height: '100%' }}>
      <FormInput
        name={'firstname'}
        control={control}
        label={'Entrez votre prénom'}
        placeholder="Prénom"
        rules={{required: true}}
      />

      <View style={FORM_GROUP}>
          <FormInput
              name={'lastname'}
              control={control}
              label={'Entrez votre Nom'}
              placeholder="Nom"
              rules={{ required: true }}
          />
      </View>
      <View style={FORM_GROUP}>
        <Controller
          control={control}
          name="sex"
          defaultValue={'Man'}
          render={({field}) => (
            <RadioInput
              {...field}
              buttonStyle={{width: '45%'}}
              multiple={false}
              list={SEX_LIST}
            />
          )}
        />
      </View>

      <View style={FORM_GROUP}>
        <FormDatePicker
          label={'Date de naissance'}
          name={'birthDate'}
          control={control}
          placeholder={'Sélectionnez une date'}
          rules={{required: true}}
        />
      </View>

      <View style={BOTTOM}>
        <CustomButton
          text="Continuer"
          onPress={handleSubmit(nextStep)}
          type={StyleType.PRIMARY}
          style={{
            marginTop: 'auto',
          }}
        />
      </View>
    </View>
  );
};

export default SignUpScreen;
