import React, {useMemo, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {StyleType, CustomButton} from '../../Form/CustomButton/CustomButton';
import {BOTTOM, FORM_GROUP} from '../../../styles/general';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import COLORS from '../../../theme/layout/colors';
import {Controller, useWatch} from 'react-hook-form';
import FormCheckBox from '../../Form/FormCheckBox/FormCheckBox';
import LocationInput from '../../Form/LocationInput/LocationInput';

export const SignUpParameters = ({
  handleSubmit,
  getValues,
  control,
  setValue,
  nextStep,
}: any) => {
  const [errorMessage, setError] = useState('');

  const notificationRadius = useWatch({
    control,
    name: 'notificationRadius',
    defaultValue: getValues('notificationRadius') ?? 5,
  });

  const notification = useWatch({
    control,
    name: 'notification',
    defaultValue: getValues('notification') ?? true,
  });

  const onRegisterPressed = data => {
    nextStep();
  };

  return (
    <View style={{height: '100%', background: 'blue'}}>
      <View style={FORM_GROUP}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Localisation</Text>
          <LocationInput name={'location'}
                         control={control}
                         rules={{ required: true}} />
        </View>
      </View>

      <View
        style={{
          width: '120%',
          transform: [{translateX: -20}],
          height: 1,
          backgroundColor: '#F3F3F3',
        }}
      />

      <View style={FORM_GROUP}>
        <TouchableOpacity
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
          onPress={() => setValue('notification', !notification)}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>
            Recevoir les notifications
          </Text>
          <FormCheckBox
            name={'notification'}
            defaultValue={true}
            control={control}
            containerStyle={{padding: 0, margin: 0}}
            checkedIcon="chevron-down"
            uncheckedIcon="chevron-up"
            size={15}
            center
            checkedColor={COLORS.primary}
          />
        </TouchableOpacity>
      </View>

      {notification && (
        <View style={FORM_GROUP}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text>Distance maximale de notification</Text>
            <Text>{notificationRadius} km</Text>
          </View>

          <Controller
            control={control}
            name="notificationRadius"
            defaultValue={[5]}
            render={({field: {value, onChange}}) => {
              return (
                <MultiSlider
                  min={0}
                  values={value}
                  selectedStyle={{backgroundColor: COLORS.primary}}
                  max={100}
                  onValuesChange={data => onChange(data)}
                  enabledTwo={false}
                />
              );
            }}
          />

          <View
            style={{
              width: '120%',
              transform: [{translateX: -20}],
              height: 1,
              backgroundColor: '#F3F3F3',
            }}
          />
        </View>
      )}

      {errorMessage.length > 0 && (
        <Text style={styles.text_error}>{errorMessage}</Text>
      )}

      <View style={BOTTOM}>
        <CustomButton
          text="Continuer"
          onPress={handleSubmit(onRegisterPressed)}
          type={StyleType.PRIMARY}
          style={{
            marginTop: 'auto',
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: 'white',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#080808',
  },
  text: {
    color: 'grey',
    marginVertical: 10,
  },
  text_error: {
    color: '#dd494e',
    padding: 2,
    margin: 2,
  },
  text_success: {
    backgroundColor: '#36D297',
    color: '#18A774',
    padding: 2,
    margin: 2,
  },
  link: {
    color: '#FDB075',
  },
});

export default SignUpParameters;
