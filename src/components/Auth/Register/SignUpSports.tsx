import React, {useEffect, useState} from 'react';
import {StyleSheet, SafeAreaView, View} from 'react-native';
import {RadioInput} from '../../Form/RadioInput/RadioInput';
import {CustomButton, StyleType} from '../../Form/CustomButton/CustomButton';
import {useDispatch, useSelector} from 'react-redux';
import {Actions} from '../../../store/ducks/search-sports';
import {BOTTOM, FORM_GROUP} from '../../../styles/general';
import Input from '../../Form/FormInput/Input';
import {Controller, useWatch} from 'react-hook-form';

export const SignUpScreen = ({control, handleSubmit, nextStep}: any) => {
  const [search, setSearch] = useState<string>('');

  const activities = useWatch({
    control,
    name: 'favoriteSports',
  });

  const dispatch = useDispatch();

  const searchData = useSelector(state => state.searchSport);

  useEffect(() => {
    dispatch(Actions.requestSearchSports());
  }, []);

  const onRegisterPressed = () => {
    nextStep();
  };

  return (
    <SafeAreaView style={styles.root}>
      <View style={FORM_GROUP}>
        <Input placeholder="Rechercher" value={search} onChange={setSearch} />
      </View>

      <Controller
        control={control}
        name="favoriteSports"
        render={({field}) => (
          <RadioInput
            {...field}
            multiple={true}
            max={3}
            list={searchData.data}
          />
        )}
      />

      {activities && activities.length > 0 && (
        <View style={BOTTOM}>
          <CustomButton
            text="Continuer"
            onPress={onRegisterPressed}
            type={StyleType.PRIMARY}
            style={{
              marginTop: 'auto',
            }}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: 'white',
    position: 'relative',
    height: '100%',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#080808',
  },
  text: {
    color: 'grey',
    marginVertical: 10,
  },
  text_error: {
    color: '#dd494e',
    padding: 2,
    margin: 2,
  },
  text_success: {
    backgroundColor: '#36D297',
    color: '#18A774',
    padding: 2,
    margin: 2,
  },
  link: {
    color: '#FDB075',
  },
});

export default SignUpScreen;
