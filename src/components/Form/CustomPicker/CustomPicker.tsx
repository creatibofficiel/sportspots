import React from "react";
import { View } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import styles from '../../../theme/components/picker';

interface ClassTypeProps {
    value: string;
    setValue: any;
    mode: "dialog" | "dropdown" | undefined;
    list: string[][]; // ex : [["blue", "blueValue"], [["red", "redValue"]]]
}

export enum StyleType { /* TODO : ? To remove ?? */
    PRIMARY = "PRIMARY",
    SECONDARY = "SECONDARY",
    TERTIARY = "TERTIARY"
}

export const CustomPicker = ({ value, setValue, mode, list }: ClassTypeProps) => {
    return (
        <View style={styles.container}>
            <Picker
                selectedValue={value}
                onValueChange={(itemValue, itemIndex) => setValue(itemValue)}
                mode={mode}>
                {list.map((item => <Picker.Item label={item[0]} value={item[1]} key={item[0]} />))}
            </Picker>
        </View >
    )
}
