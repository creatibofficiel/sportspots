import React from "react";
import { Control, useController } from "react-hook-form";
import { CheckBox } from "react-native-elements";

interface ClassTypeProps {
    defaultValue?: string;
    name: string;
    control: Control;
}

const FormCheckBox = ({ control, name, defaultValue = '', ...rest }: any & ClassTypeProps) => {
    const { field: { onChange, value } } = useController({
        control, name, defaultValue
    });

    return (
        <CheckBox
            checked={value}
            onPress={() => onChange(!value)}
            {...rest}
        />
    )
}

export default FormCheckBox;
