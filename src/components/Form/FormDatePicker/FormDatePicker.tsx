import React, {useState} from 'react';
import { View, Text } from 'react-native';
import styles from '../../../theme/components/input';
import {Control, useController, ValidationValueMessage} from 'react-hook-form';
import DatePicker from 'react-native-datepicker';

interface ClassTypeProps {
  defaultValue?: string;
  placeholder: any;
  name: string;
  rules?: Pick<string | boolean | ValidationValueMessage<boolean>, never>;
  control: Control;
  secureTextEntry?: boolean;
  multiline?: boolean;
  label?: string;
  format?: string;
  mode?: "date" | "datetime";
}

const FormDatePicker = ({
  control,
  placeholder,
  label,
  name,
  rules = {},
  defaultValue = '',
  format = "DD-MM-YYYY",
  mode = "date",
}: ClassTypeProps) => {

  const {
    field: {onChange, value},
    fieldState,
  } = useController({
    control,
    rules,
    name,
    defaultValue,
  });

  return (
    <View style={styles.container}>
      {label && <Text style={styles.text}>{label}</Text>}
      <DatePicker
        style={styles.input}
        mode={mode}
        locale={'fr'}
        date={value}
        placeholder={placeholder}
        format={format}
        androidMode={'spinner'}
        confirmBtnText="Confirmer"
        cancelBtnText="Annuler"
        customStyles={{
          dateInput: {
            borderWidth: 0,
          },
        }}
        showIcon={false}
        onDateChange={date => onChange(date)}
      />
    </View>
  );
};

export default FormDatePicker;
