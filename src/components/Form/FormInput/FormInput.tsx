import React, { useState } from "react";
import {View, Text, TextInput, ViewStyle, TextInputProps} from 'react-native';
import styles from '../../../theme/components/input';
import { Control, useController, ValidationValueMessage} from "react-hook-form";
import COLORS from "../../../theme/layout/colors";

interface ClassTypeProps extends TextInputProps {
    defaultValue?: string;
    placeholder: any;
    name: string;
    rules?: Pick<string|boolean|ValidationValueMessage<boolean>, never>;
    control: Control;
    secureTextEntry?: boolean;
    multiline?: boolean;
    label?: string
}

const FormInput = ({ control, placeholder, label, name, secureTextEntry = false, multiline = false, rules = {}, defaultValue = '', ...props }: ClassTypeProps) => {
    const [focus, setFocus] = useState<boolean>(false);

    const { field: { onChange, value }, fieldState } = useController({
        control, rules, name, defaultValue
    });

    return (
        <View style={styles.container}>
            {label && <Text style={styles.text}>{label}</Text>}
                <TextInput
                    value={value}
                    onChangeText={onChange}
                    placeholder={placeholder}
                    style={[styles.input, focus ? styles.inputFocus : {}, (fieldState.isDirty || control._formState.isSubmitted) && fieldState.invalid ? styles.inputInvalid : {}]}
                    secureTextEntry={secureTextEntry}
                    multiline={multiline}
                    onFocus={() => setFocus(true)}
                    onBlur={() => setFocus(false)}
                    {...props}
                />
            {fieldState.invalid && fieldState.error?.message !== "" && <Text style={{ color: COLORS.danger }}>{ fieldState.error?.message }</Text>}
        </View>
    )
}

export default FormInput;
