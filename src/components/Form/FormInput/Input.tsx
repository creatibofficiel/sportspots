import React, { useState } from "react";
import { View, Text, TextInput } from 'react-native';
import styles from '../../../theme/components/input';

interface ClassTypeProps {
    value: string;
    onChange: any;
    placeholder: any;
    secureTextEntry?: boolean;
    multiline?: boolean;
    label?: string
}

const Input = ({ value, onChange, placeholder, label, secureTextEntry = false, multiline = false }: ClassTypeProps) => {
    const [focus, setFocus] = useState<boolean>(false);

    return (
        <View style={styles.container}>
            {label && <Text style={styles.text}>{label}</Text>}
                <TextInput
                    value={value}
                    onChangeText={onChange}
                    placeholder={placeholder}
                    style={[styles.input, focus ? styles.inputFocus : {}]}
                    secureTextEntry={secureTextEntry}
                    multiline={multiline}
                    onFocus={() => setFocus(true)}
                    onBlur={() => setFocus(false)}
                />
        </View>
    )
}

export default Input;
