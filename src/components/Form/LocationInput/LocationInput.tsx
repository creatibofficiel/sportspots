import React, { useState } from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';
import styles from '../../../theme/components/input';
import Geocode from '../../Geocode/Geocode';
import { MapBoxGeocodable } from '../../../services/geocoding/MapBoxGeocodable';
import GetLocation, {Location} from 'react-native-get-location';
import { useController } from 'react-hook-form';
import COLORS from "../../../theme/layout/colors";

const LocationInput = ({
  name,
  control,
  rules = {},
  defaultValue = null,
}: any) => {
  const {
    field: {onChange, value},
    fieldState,
  } = useController({
    control,
    name,
    defaultValue,
    rules,
  });

  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [locationAddress, setLocationAddress] = useState<string>();
  const [isSelfLocation, setSelfLocation] = useState<any>(null);

  const getLocation = () => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 20000,
    })
      .then((location: Location) => {
        setModalVisible(false);
        setSelfLocation(true);
        onChange({
          lat: location.latitude,
          lng: location.longitude,
        });
      })
      .catch(error => {
        const {code, message} = error;
        setError(message);
      });
  };

  return (
    <View>
      <Modal visible={modalVisible}>
        <View style={{ padding: 20 }}>
          <TouchableOpacity onPress={getLocation}>
            <Text>Utiliser la localisation de mon appareil</Text>
          </TouchableOpacity>
          {error !== '' && error}
          <Text>Ou recherchez votre position</Text>
          <Geocode
            provider={new MapBoxGeocodable()}
            onChange={(location: any) => {
              setModalVisible(false);
              setSelfLocation(false);
              setLocationAddress(location.address);
              onChange(location.location);
            }}
          />
        </View>
      </Modal>

      <TouchableOpacity onPress={() => setModalVisible(true)}>
        <Text style={fieldState.invalid ? { color: COLORS.danger } : {}}>
          {value !== null
            ? isSelfLocation
              ? 'Votre position'
              : locationAddress
            : 'Définissez une position'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default LocationInput;
