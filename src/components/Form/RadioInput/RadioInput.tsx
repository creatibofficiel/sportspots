import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styles from '../../../theme/components/radio-input';

interface ItemType {
  name: string;
  icon?: string;
  id: string;
}

interface RadioInputType {
  value: string[] | string;
  onChange: any;
  list: ItemType[];
  multiple: boolean;
  max?: number;
  buttonStyle?: any;
}

export const RadioInput = ({
  onChange,
  list,
  value = [],
  multiple = false,
  max,
  buttonStyle,
}: RadioInputType) => {
  const [values, setValues] = useState<string[] | string>(
    value ? (Array.isArray(value) ? value : multiple ? [value] : value) : [],
  );

  const onPress = (id: string) => {
    if (values.includes(id)) {
      if (Array.isArray(values)) {
        setValues(values.filter(item => item !== id));
      } else {
        setValues(id);
      }
    } else {
      if (multiple) {
        if (!max || values.length < max || values.length === 0) {
          const array = JSON.parse(JSON.stringify(values));
          array.push(id);
          setValues(array);
        }
      } else {
        setValues(id);
      }
    }
  };

  useEffect(() => {
    onChange(values);
  }, [values]);

  return (
    <View style={styles.container}>
      {list.map((item: ItemType) => (
        <TouchableOpacity
          key={item.id}
          activeOpacity={0.7}
          style={[
            styles.input,
            buttonStyle ?? {},
            values.includes(item.id) ? styles.selectedInput : {},
          ]}
          onPress={() => onPress(item.id)}>
          {item.icon && <Image source={{uri: item.icon}} />}
          <Text
            style={
              values.includes(item.id) ? styles.selectedText : styles.text
            }>
            {item.name}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};
