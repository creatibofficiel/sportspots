import React, { useEffect, useState } from "react";
import {Text, FlatList, View, TouchableOpacity} from 'react-native';
import Input from "../Form/FormInput/Input";
import { Geocodable, GeocodeResultItem } from "../../services/geocoding/Geocodable";
import debounce from "../../utils/debounce";

interface GeocodePropsType {
    provider: Geocodable
    onChange: any
}

const GeocodeItem = ({ item }: { item: GeocodeResultItem}) => {
    return <View style={{ padding: 10 }}>
        <Text>{item.address}</Text>
    </View>
}

const Geocode = ({ provider, onChange }: GeocodePropsType) => {
    const [search, setSearch] = useState<string>("");
    const [results, setResults] = useState<GeocodeResultItem[]>([]);

    useEffect(() => {
        if(search !== "") debounce( 1000, () => {
            provider.getResults(search).then(setResults)
        })
    }, [search]);

    return (
        <>
            <Input
                label={"Rechercher"}
                placeholder={"Adresse"}
                value={search}
                onChange={setSearch}>
            </Input>

            <FlatList
                data={results}
                renderItem={ ({ item }) => <TouchableOpacity activeOpacity={.6} onPress={() => onChange(item)}><GeocodeItem item={item} /></TouchableOpacity>}>
            </FlatList>
        </>
    )
};

export default Geocode;
