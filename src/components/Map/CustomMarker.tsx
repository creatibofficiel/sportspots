import React from "react";
import {View, StyleSheet} from "react-native";
import { MapEvent, Marker } from "react-native-maps";
import {SvgUri} from "react-native-svg";
import COLORS from "../../theme/layout/colors";

interface CustomMarkerType {
    item: ItemType,
    isSelected: boolean,
    onPress: (event: MapEvent<{ action: 'marker-press'; id: string }>) => void,
}

interface ItemType {
    name: string;
    latitude: number,
    longitude: number,
    icon?: string;
}

const CustomMarker = ({ item, isSelected, onPress }: CustomMarkerType ) => {
    return (
        <Marker coordinate={{ latitude: item.latitude, longitude: item.longitude }} onPress={onPress}>
            <View style={{
                padding: 5,
                backgroundColor: COLORS.primary,
                borderTopRightRadius: 15,
                borderTopLeftRadius: 15,
                borderBottomRightRadius: 15,
                borderBottomLeftRadius: 15,
            }}>
                {item.icon && <SvgUri width={20} height={20} uri={item.icon} fill={"white"} style={{ width: 20, height: 20 }} />}
            </View>
            <View style={[styles.triangle]} />
        </Marker>
    );
};

const styles = StyleSheet.create({
    triangle: {
        width: 0,
        transform: [{
            rotate: "180deg",
        }, {
            translateY: 5,
        }, {
            translateX: -3
        }],
        height: 0,
        backgroundColor: "transparent",
        borderStyle: "solid",
        borderLeftWidth: 12,
        borderRightWidth: 12,
        borderBottomWidth: 20,
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: COLORS.primary,
    },
});

export default CustomMarker;
