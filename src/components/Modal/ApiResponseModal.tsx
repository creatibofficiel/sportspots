import {ActivityIndicator, GestureResponderEvent, Modal, Text, View} from "react-native";
import COLORS from "../../theme/layout/colors";
import { Icon} from "react-native-elements";
import { CustomButton } from "../Form/CustomButton/CustomButton";
import React, {forwardRef, useImperativeHandle, useState} from "react";

export const ApiResponseModal = React.forwardRef(( { loadingTitle, successCallback }: any, ref) => {
    const [visible, setVisible] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [success, setSuccess] = useState<boolean>(false);
    const [title, setTitle] = useState<string>('');
    const [descriptionMessages, setDescriptionMessages] = useState<string[]>([]);

    const onClose = () => {
        setVisible(false)
        if(success && successCallback) successCallback();
    }

    useImperativeHandle(ref, () => ({
        open: (loading: boolean = true) => {
            setVisible(true);
            setLoading(loading);
        },
        update: (loading: boolean = false, success: boolean, title: string, descriptionMessages: string[] = []) => {
            setSuccess(success);
            setTitle(title);
            setDescriptionMessages(descriptionMessages);
            setLoading(loading);
        },
        close: () => {
            setVisible(false);
            setLoading(false);
        }
    }));

    return (
        <Modal
            visible={visible}
            animationType="fade"
            transparent={true}
        >
            <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%', backgroundColor: 'rgba(0, 0, 0, 0.6)' }}>
                <View style={{ backgroundColor: 'white', width: '90%', height: '50%', borderRadius: 30, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                    {loading && <View style={{ padding: 20 }}>
                        <View>
                            <ActivityIndicator size="large" color={COLORS.primary} />
                            <Text style={{ textAlign: 'center', fontSize: 20, marginTop: 20 }}>{loadingTitle}</Text>
                        </View>
                    </View>}
                    {!loading && <View style={{ padding: 20}}>
                        <View>
                            <Icon type="material" name={success ? "check" : "error-outline"} size={60} color={success ? COLORS.primary : COLORS.danger }/>
                            <Text style={{ textAlign: 'center', fontSize: 18, color: success ? COLORS.primary : COLORS.danger }}>{title}</Text>

                            <View style={{ marginTop: 20 }}>
                                {descriptionMessages && descriptionMessages.map((msg: string, index: number) => <Text key={index}>{msg}</Text>)}
                            </View>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <CustomButton
                                onPress={onClose}
                                text={"C'est compris"} />
                        </View>
                    </View>}
                </View>
            </View>
        </Modal>
    )
});
