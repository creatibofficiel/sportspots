import React, { useEffect } from "react";
import {StyleSheet, Text, useWindowDimensions, View, ViewProps} from "react-native";
import Animated, { SharedValue, useAnimatedStyle, useSharedValue, withRepeat, withTiming } from "react-native-reanimated";
import LinearGradient from 'react-native-linear-gradient';

export const LoadSkeleton = ({ loading, children }: { children: JSX.Element[], loading: boolean }) => {
    const { width } = useWindowDimensions();

    const progress = useSharedValue(-80);

    const renderChild = (e: JSX.Element, index: number): JSX.Element => {
        if(e.type === View) return <View style={e.props.style ?? {}}>
            {renderChildren(e.props.children)}
        </View>;
        return <SkeletonView key={index} progress={progress} style={e.props.style ?? {}} />;
    }
    const renderChildren = (children: any) => {
        let elements: JSX.Element[] = [];

        if(React.isValidElement(children)) {
            elements.push(renderChild(children, 0));
        } else {
            children.map((e: JSX.Element, index: number) => {
                elements.push(renderChild(e, index));
            })
        }

        return elements;
    }

    useEffect(() => {
        progress.value = -80;
        progress.value = withRepeat(withTiming(width, {
            duration: 1000,
        }), -1, false);
    });

    return (
      <View>
          {loading && renderChildren(children)}
          {!loading && children}
      </View>
    );
};

const SkeletonView = ({ style, progress }: { style: ViewProps, progress: SharedValue<number> }) => {
    let width = Math.random() * 100;
    if(width < 30) width = 30;
    const contentWidth = width + "%";

    const AnimatedLinearGradient = Animated.createAnimatedComponent(LinearGradient);

    const animatedStyle = useAnimatedStyle(() => ({
        transform: [{
            translateX: progress.value
        }]
    }));

    return (
        <View
            style={[{ width: contentWidth, height: 25 }, style, styles.skeletonView]}
        >
            <AnimatedLinearGradient
                colors={["#E1E9EE", "#f5f5f5", "#E1E9EE"]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={[{width: 120}, StyleSheet.absoluteFillObject, animatedStyle]}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    skeletonView: {
        overflow: 'hidden',
        backgroundColor: '#E1E9EE',
        borderColor: "#b0b0b0",
        borderRadius: 4,
    }
});
