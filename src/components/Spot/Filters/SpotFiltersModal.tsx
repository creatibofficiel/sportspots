import { Modal, Text, TouchableOpacity, View } from "react-native";
import COLORS from "../../../theme/layout/colors";
import { BOTTOM, FORM_GROUP } from "../../../styles/general";
import LocationInput from "../../Form/LocationInput/LocationInput";
import { Controller, useWatch } from "react-hook-form";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { RadioInput } from "../../Form/RadioInput/RadioInput";
import { CustomButton, StyleType } from "../../Form/CustomButton/CustomButton";
import React, { useEffect } from "react";
import useTrans from "../../../hooks/translation";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../../../store/ducks/search-sports";
import { CloseIcon } from "../../Svg/CloseIcon";

interface FiltersModalType {
    open: boolean;
    control: any;
    closeModal: any;
    handleSubmit: any;
    getValues: any;
}

export const SpotFiltersModal = ({ open, closeModal, control, getValues, handleSubmit }: FiltersModalType ) => {
    const { t } = useTrans();

    const dispatch = useDispatch();

    const distanceRadius = useWatch({
        control,
        name: 'radius',
        defaultValue: getValues('radius') ?? 5,
    });

    const sportsData = useSelector(state => state.searchSport);

    useEffect(() => {
        dispatch(Actions.requestSearchSports());
    }, []);

    return <Modal visible={open}>
        <View style={{ position: 'absolute', zIndex: 10, top: '5%', paddingHorizontal: 30, width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontSize: 22, fontWeight: 'bold', color: '#000B22' }}>{t('Filters')}</Text>
            <TouchableOpacity onPress={closeModal} activeOpacity={.7}>
                <CloseIcon color={COLORS.primary} width={24} height={24} />
            </TouchableOpacity>
        </View>
        <View style={{ height: '100%', marginTop: 30 }}>
            <View style={FORM_GROUP}>
                <View
                    style={{
                        marginTop: 50,
                        paddingHorizontal: 25,
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>A proximité de</Text>
                    <LocationInput name={'location'}
                                   control={control}
                                   rules={{ required: true}} />
                </View>
                <View style={{
                    marginTop: 50,
                    paddingHorizontal: 25,
                }}>
                    <Text style={{color: '#8d8d8d'}}>
                        Distance maximal depuis votre position : {distanceRadius} km
                    </Text>
                    <Controller
                        control={control}
                        name="radius"
                        defaultValue={[5]}
                        render={({field: {value, onChange}}) => {
                            return (
                                <MultiSlider
                                    min={0}
                                    values={value}
                                    selectedStyle={{backgroundColor: COLORS.primary}}
                                    max={100}
                                    onValuesChange={data => onChange(data)}
                                    enabledTwo={false}
                                />
                            );
                        }}
                    />
                </View>
                <View style={{
                    marginTop: 50,
                    paddingHorizontal: 25,
                }}>
                    <Text style={{
                        fontSize: 15,
                    }}>Activités</Text>
                    <Controller
                        control={control}
                        name="activities"
                        render={({field}) => (
                            <RadioInput
                                {...field}
                                multiple={true}
                                max={3}
                                list={sportsData.data}
                            />
                        )}
                    />
                </View>
            </View>
            <View style={BOTTOM}>
                <CustomButton
                    text="Rechercher"
                    onPress={handleSubmit}
                    type={StyleType.PRIMARY}
                    style={{
                        marginLeft: "15%",
                        width: "70%",
                        marginVertical: 25,
                    }}
                />
            </View>
        </View>
    </Modal>
}
