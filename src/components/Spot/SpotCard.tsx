import React from "react";
import {View, Text, Image, StyleSheet, useWindowDimensions, Pressable} from "react-native";
import {CommonActions, useNavigation} from "@react-navigation/native";
import Spot from '../../models/Spot';
import {SharedElement} from "react-navigation-shared-element";

interface SpotCarrouselType {
    spot: Spot;
}

const SpotCard = ({ spot }: SpotCarrouselType ) => {
    const navigation = useNavigation();

    const goToSpotPage = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'SpotDetail',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                    spot: spot,
                },
            })
        );
    }

    return (
        <Pressable onPress={goToSpotPage} style={styles.container}>
            <SharedElement id={`spot.${spot.id}.image`}>
                <Image
                    source={{ uri: spot.image }}
                    style={styles.image}
                />
            </SharedElement>

            <View style={{
                flex: 1,
                marginHorizontal: 15,
            }}>
                <SharedElement id={`spot.${spot.id}.sport`}>
                    <Text style={styles.people}>
                        { spot.sport }
                    </Text>
                </SharedElement>

                <Text style={styles.description} numberOfLines={2}>
                    {spot.spotDesc}
                </Text>

                {/*Localisation*/}
                <Text style={styles.localisation}>
                    {spot.spotPostalCode}, {spot.spotCityName}
                </Text>
            </View>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 125,
        width: '100%',
        padding: 15,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 16,
    },
    image: {
        height: '100%',
        backgroundColor: 'white',
        aspectRatio: 1,
        resizeMode: "cover",
        borderRadius: 12
    },
    people: {
        color: '#707070',
    },
    description: {
        fontSize: 15,
        color: '#707070',
        marginTop: 8
    },
    time: {
        fontSize: 18,
        color: '#7b7b7b',
        marginVertical: 10,
    },
    date: {
        fontWeight: "bold",
    },
    localisation: {
        color: '#7b7b7b',
        textDecorationLine: "underline",
    },
});

export default SpotCard;
