import React, {useEffect} from "react";
import {Text, Image, StyleSheet, ScrollView, View, FlatList} from "react-native";
import { SharedElement } from "react-navigation-shared-element";
import {useDispatch, useSelector} from "react-redux";
import { Actions as SpotActions } from '../../store/ducks/spot';

interface SpotCommentType {
    spotComment: any;
}

const SpotComment = ({ spotComment }: SpotCommentType) => {
    return (
        <View>
            {spotComment.author.fullname} {/* TODO : show user profile ^^ */}
            {spotComment.comment}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        aspectRatio: 3 / 2,
        resizeMode: "cover",
        backgroundColor: 'white',
        borderRadius: 10,
    },
    people: {
        marginVertical: 10,
        color: '#7b7b7b',
    },
    description: {
        fontSize: 18,
        lineHeight: 26,
    },
    time: {
        fontSize: 18,
        color: '#7b7b7b',
        marginVertical: 10,
    },
    date: {
        fontWeight: "bold",
    },
    localisation: {
        color: '#7b7b7b',
        textDecorationLine: "underline",
    },
    longDescription: {
        fontSize: 16,
        lineHeight: 24,
        marginVertical: 20,
    },
});

export default SpotComment;
