import React, {useEffect, useState} from "react";
import {Text, Image, StyleSheet, View, FlatList} from "react-native";
import { SharedElement } from "react-navigation-shared-element";
import {useDispatch, useSelector} from "react-redux";
import { Actions as SpotActions } from '../../store/ducks/spot';
import { Actions as SpotCommentActions } from '../../store/ducks/spot-comments';
import SpotComment from "./SpotComment";
import {LoadSkeleton} from "../Skeleton/LoadSkeleton";

interface DetailedSpotType {
    route: any;
}

const SpotDetail = ({route}: DetailedSpotType) => {

    const { spot } = route.params;

    const dispatch = useDispatch();

    const [page, setPage] = useState<number>(1);

    const spotDetail = useSelector(state => state.spot);
    const comments = useSelector(state => state.spotComments);

    useEffect(() => {
        if(spotDetail.data.id !== spot.id) {
            dispatch(SpotActions.requestSpotDetailRequest(spot.id));
        }
    }, [])

    const fetchMoreComments = () => {
        setPage(page+1);
    }

    useEffect(() => {
        dispatch(SpotCommentActions.requestSpotCommentsRequest(spot.id, page));
    }, [page]);

    return (
        <View style={styles.container}>
            <SharedElement id={`spot.${spot.id}.image`}>
                <Image
                    source={{ uri: spot.image }}
                    style={styles.image}
                />
            </SharedElement>

            <View style={{ borderRadius: 30, padding: 30, transform: [{ translateY: -27 }], backgroundColor: '#F3F3F3', zIndex: 80 }}>
                <SharedElement id={`spot.${spot.id}.sport`}>
                    <Text style={styles.people}>
                        { spot.sport }
                    </Text>
                </SharedElement>

                <LoadSkeleton loading={spotDetail.loading}>
                    <Text>{ spotDetail.data.description }</Text>
                    <Text style={{ marginTop: 10 }}>{ spotDetail.data.address }</Text>
                    <Text style={{ marginTop: 10 }}>{ spotDetail.data.latitude }</Text>
                    <Text style={{ marginTop: 10 }}>{ spotDetail.data.longitude }</Text>
                    <Text style={{ marginTop: 10 }}>{ spotDetail.data.author.fullname }</Text>
                    <Text style={{ marginTop: 10, width: '20%' }}>{ spotDetail.data.createdAt }</Text>
                </LoadSkeleton>

                <Text>Commentaires ({comments.data.length})</Text>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1 }}
                    data={comments.data}
                    renderItem={(item) => <SpotComment spotComment={item} key={item.id} />}
                    onEndReachedThreshold={0.2}
                    onEndReached={fetchMoreComments}>
                </FlatList>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        aspectRatio: 3 / 2,
        resizeMode: "cover",
        backgroundColor: 'white',
    },
    people: {
        marginVertical: 10,
        color: '#7b7b7b',
    },
    description: {
        fontSize: 18,
        lineHeight: 26,
    },
    time: {
        fontSize: 18,
        color: '#7b7b7b',
        marginVertical: 10,
    },
    date: {
        fontWeight: "bold",
    },
    localisation: {
        color: '#7b7b7b',
        textDecorationLine: "underline",
    },
    longDescription: {
        fontSize: 16,
        lineHeight: 24,
        marginVertical: 20,
    },
});

export default SpotDetail;
