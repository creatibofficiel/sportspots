import Svg, {Path} from "react-native-svg";
import React from "react";
import COLORS from "../../theme/layout/colors";

export const CloseIcon = ({ color, width = 15, height = 15 }: { color: string, width?: number, height?: number }) => {
    return (<Svg width={width} height={height}>
        <Path
            fill={color}
            d={"M18.717 6.697l-1.414-1.414-5.303 5.303-5.303-5.303-1.414 1.414 5.303 5.303-5.303 5.303 1.414 1.414 5.303-5.303 5.303 5.303 1.414-1.414-5.303-5.303z"} />
    </Svg>);
}
