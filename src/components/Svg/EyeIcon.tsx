import Svg, {Circle, Line, Path, Polygon} from "react-native-svg";
import React from "react";

export const EyeIcon = ({ color, width = 15, height = 15, visible = false }: { color: string, width?: number, height?: number, visible: boolean }) => {
    return (<Svg width={width} height={height}>
        <Path
            fill={color}
            d={"M10 7.5a2.5 2.5 0 1 0 2.5 2.5A2.5 2.5 0 0 0 10 7.5zm0 7a4.5 4.5 0 1 1 4.5-4.5 4.5 4.5 0 0 1-4.5 4.5zM10 3C3 3 0 10 0 10s3 7 10 7 10-7 10-7-3-7-10-7z"} />
        {!visible && <Line x1={0} y1={width} x2={width} y2={0} stroke={color} strokeWidth="2" />}
    </Svg>);
}
