import Svg, {Path} from "react-native-svg";
import React from "react";

export const FiltersIcon = ({ color, width = 15, height = 15 }: { color: string, width?: number, height?: number }) => {
    return (<Svg width={width} height={height}>
        <Path
            fill={color}
            transform={"translate(0 -76.5)"}
            d={"M4.667,84.5H7.333V83.167H4.667ZM0,76.5v1.333H12V76.5Zm2,4.667h8V79.833H2Z"}>
        </Path>
    </Svg>);
}
