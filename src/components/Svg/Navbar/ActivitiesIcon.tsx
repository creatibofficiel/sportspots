import COLORS from "../../../theme/layout/colors";
import React from "react";
import Animated from "react-native-reanimated";
import Svg, {Path} from "react-native-svg";

const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const AnimatedPath = Animated.createAnimatedComponent(Path);

export const ActivitiesIcon = (svgProps: any, pathProps: any, focused: any) => {
    return (<AnimatedSvg animatedProps={svgProps}>
        <AnimatedPath
            animatedProps={pathProps}
            fill={focused ? COLORS.primary : "#B0B3BA"}
            transform={"translate(-156.801 -75.605)"}
            d={"M166.584,87.042a.859.859,0,0,0-.23-1.7,2.364,2.364,0,0,1-.315.021,2.32,2.32,0,1,1,2.32-2.32,2.352,2.352,0,0,1-.03.377.859.859,0,0,0,1.7.277,4.036,4.036,0,1,0-3.442,3.349Z"}>
        </AnimatedPath>
        <AnimatedPath
            animatedProps={pathProps}
            fill={focused ? COLORS.primary : "#B0B3BA"}
            transform={"translate(-41 -359.84)"}
            d={"M58.091,376.882a10.422,10.422,0,0,0-2.7-.868.859.859,0,0,0-.342,1.684,5.854,5.854,0,0,1,2.661.984,4.68,4.68,0,0,1-2.15.892,23.722,23.722,0,0,1-5.325.547,23.722,23.722,0,0,1-5.325-.547,4.68,4.68,0,0,1-2.15-.892,5.854,5.854,0,0,1,2.661-.984.859.859,0,0,0-.342-1.684,10.423,10.423,0,0,0-2.7.868,2.117,2.117,0,0,0-1.385,1.8c0,1.066,1.038,1.892,3.086,2.453a24.3,24.3,0,0,0,6.152.7,24.3,24.3,0,0,0,6.152-.7c2.048-.561,3.086-1.387,3.086-2.453A2.117,2.117,0,0,0,58.091,376.882Z"}>
        </AnimatedPath>
        <AnimatedPath
            animatedProps={pathProps}
            fill={focused ? COLORS.primary : "#B0B3BA"}
            transform={"translate(-81.195)"}
            d={"M89.707,18.162a.859.859,0,0,0,1.453,0,23.353,23.353,0,0,1,2.805-3.4c1.938-2.066,3.942-4.2,3.945-7.375a7.309,7.309,0,0,0-2.2-5.244,7.531,7.531,0,0,0-10.533,0A7.338,7.338,0,0,0,83,7.391c0,3.176,2,5.316,3.926,7.386A23.612,23.612,0,0,1,89.707,18.162Zm.726-16.444A5.727,5.727,0,0,1,96.191,7.39c0,2.492-1.614,4.21-3.48,6.2a32.7,32.7,0,0,0-2.278,2.61,33.077,33.077,0,0,0-2.251-2.593c-1.858-1.994-3.462-3.716-3.464-6.214A5.638,5.638,0,0,1,90.434,1.719Z"}>
        </AnimatedPath>
    </AnimatedSvg>);
}
