import COLORS from "../../../theme/layout/colors";
import React from "react";
import Animated from "react-native-reanimated";
import Svg, {Path} from "react-native-svg";

const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const AnimatedPath = Animated.createAnimatedComponent(Path);

export const HomeIcon = (svgProps: any, pathProps: any, focused: any) => {
    return (<AnimatedSvg animatedProps={svgProps}>
        <AnimatedPath
            animatedProps={pathProps}
            fill={focused ? COLORS.primary : "#B0B3BA"}
            transform={"translate(0 -10.492)"}
            d={"M22.739,20.615l-9.361-9.4-.037-.035a2.7,2.7,0,0,0-3.593-.008l-.037.035-9.447,9.4A.9.9,0,1,0,1.533,21.88l.713-.71v7.737A3.593,3.593,0,0,0,5.84,32.492H9.074a.9.9,0,0,0,.9-.9v-7.3h3.145v7.3a.9.9,0,0,0,.9.9H17.16a3.593,3.593,0,0,0,3.594-3.584.9.9,0,0,0-1.8,0,1.8,1.8,0,0,1-1.8,1.792H14.914V23.4a.9.9,0,0,0-.9-.9H9.074a.9.9,0,0,0-.9.9v7.3H5.84a1.8,1.8,0,0,1-1.8-1.792v-9.5q0-.013,0-.027L10.959,12.5a.9.9,0,0,1,1.166,0l6.833,6.86v5.067a.9.9,0,0,0,1.8,0V21.164l.71.713a.9.9,0,0,0,1.275-1.263Z"}>
        </AnimatedPath>
    </AnimatedSvg>);
}
