import {useTranslation} from "react-i18next";

import i18n from "../i18n/config";
const initI18n = i18n;

export default function useTrans() {
    return useTranslation();
}
