import i18n from 'i18next';
import fr from './fr.json';
import es from './es.json';
import {initReactI18next} from 'react-i18next';
/*import {getLocales} from 'react-native-localize';*/

i18n.use(initReactI18next).init({
    lng: 'fr',
    fallbackLng: 'en',
    resources: {
        fr,
        es
    },
});

export default i18n;
