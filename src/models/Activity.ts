import Coordinate from './Coordinate';

export default interface Activity {
    id: string;
    name: string;
    activity: string;
    description: string;
    address: string;
    maxParticipants: number;
    author: string;
    createdAt: string;
    spotId: string;
    image: string;
    coordinates: Coordinate,
}
