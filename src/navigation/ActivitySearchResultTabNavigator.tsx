import React from "react";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import ListActivityScreen from "../screens/Activity/ListActivityScreen";
import ListActivityMapScreen from "../screens/Activity/ListActivityMapScreen";

const Tab = createMaterialTopTabNavigator();

const ActivitySearchResultTabNavigator = (props:any) => {
    return (
      <Tab.Navigator screenOptions={
          {
             tabBarActiveTintColor: '#0f7eaa',
              tabBarIndicatorStyle: {
                 backgroundColor: '#e7e7e7',
              }
          }
      }
      >
          <Tab.Screen name={'Map'} component={ListActivityMapScreen} />
          <Tab.Screen name={'Liste'} component={ListActivityScreen} />
      </Tab.Navigator>
    );
};

export default ActivitySearchResultTabNavigator;
