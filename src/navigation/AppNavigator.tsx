import React, { Component } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View } from 'react-native';

import SettingsNavigator from "./SettingsNavigator";
import HomeNavigator from "./HomeNavigator";
import ActivityNavigator from "./ActivityNavigator";
import COLORS from "../theme/layout/colors";

import { useAnimatedProps, withSpring } from 'react-native-reanimated';
import {HomeIcon} from "../components/Svg/Navbar/HomeIcon";
import {ActivitiesIcon} from "../components/Svg/Navbar/ActivitiesIcon";
import {ParametersIcon} from "../components/Svg/Navbar/ParametersIcon";

const Tab = createBottomTabNavigator();

const CustomTabBarButton = (icon: string, width: number, height: number, iconSvg: any) => {
    return ({ focused }: any) => {
        const w = width;
        const h = height;

        const svgProps = useAnimatedProps(() => ({
            width: withSpring(focused ? w*1.2 : w),
            height: withSpring(focused ? h*1.2 : h),
        }));

        const pathProps = useAnimatedProps(() => ({
            scale: withSpring(focused ? 1.2 : 1),
        }));

        return (
            <View style={{height: '100%', alignItems: 'center'}}>
                {iconSvg(svgProps, pathProps, focused)}
                {focused && <View style={{
                    width: 5,
                    height: 5,
                    borderRadius: 50,
                    backgroundColor: COLORS.primary,
                    marginTop: 10
                }}/>}
            </View>
        );
    }
};

const AppNavigator = () => {
    return (
        <Tab.Navigator initialRouteName="HomeNavigator" screenOptions={{
            tabBarActiveTintColor: COLORS.primary,
            tabBarStyle: {
                borderTopLeftRadius: 35,
                borderTopRightRadius: 35,
                backgroundColor: "white",
                position: 'absolute',
                bottom: 0,
                padding: 0,
                width: '100%',
                height: 90,
                paddingTop: 20
            },
            tabBarItemStyle: {
                display: 'flex',
                justifyContent: 'flex-start',
                marginBottom: 25,
            },
        }}>
            <Tab.Screen name="HomeNavigator" component={HomeNavigator} options={{
                tabBarShowLabel: false,
                headerShown: false,
                tabBarIcon: CustomTabBarButton('home',23, 22, HomeIcon)
            }} />

            <Tab.Screen name="ActivityNavigator" component={ActivityNavigator} options={{
                tabBarShowLabel: false,
                headerShown: false,
                tabBarIcon: CustomTabBarButton('event', 18.48, 22, ActivitiesIcon)
            }} />

            <Tab.Screen name="SettingsNavigator" component={SettingsNavigator} options={{
                tabBarShowLabel: false,
                headerShown: false,
                tabBarIcon: CustomTabBarButton('settings', 22, 22, ParametersIcon)
            }} />
        </Tab.Navigator>
    );
}

export default AppNavigator;
