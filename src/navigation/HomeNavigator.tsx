import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import ListSpotScreen from "../screens/Spot/ListSpotScreen";
import SpotDetail from "../components/Spot/SpotDetail";
import {createSharedElementStackNavigator} from "react-navigation-shared-element";

const Stack = createStackNavigator();

const AnimatedStack = createSharedElementStackNavigator();

const HomeNavigator = () => {
    return (
      <AnimatedStack.Navigator initialRouteName={"Map"} screenOptions={{ headerShown: false }}>
          <AnimatedStack.Screen
              name={'Map'}
              component={ListSpotScreen}
              options={{
                  headerShown: false,
              }}
              />
          <AnimatedStack.Screen
              name={'SpotDetail'}
              component={SpotDetail}
              options={{
                  headerShown: false,
                  gestureEnabled: false,
                  cardStyleInterpolator: ({ current: { progress } }) => ({
                      cardStyle: {
                          opacity: progress,
                      },
                  }),
              }}
              sharedElements={(route: any, otherRoute: any, showing: any) => {
                  const { spot } = route.params;
                  return [`spot.${spot.id}.image`, {id: `spot.${spot.id}.sport`, resize: 'clip'}];
              }}
          />
      </AnimatedStack.Navigator>
    );
};

export default HomeNavigator;
