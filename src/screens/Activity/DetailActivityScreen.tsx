import React from "react";
import {View, FlatList} from "react-native";
import ActivityComponent from "../../components/Activity/ActivityComponent";

const SearchResultScreen = () => {
    return (
      <View>
          <FlatList
              data={[]}
              renderItem={({item}) => <ActivityComponent activity={item} />}
              />
      </View>
    );
};

export default SearchResultScreen;
