import React, { createRef, useEffect, useState } from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import MapView, { Coordinate, PROVIDER_GOOGLE, Region } from 'react-native-maps';
import GetLocation, { Location } from 'react-native-get-location'
import { useIsFocused } from "@react-navigation/core";
import COLORS from "../../theme/layout/colors";
import { useForm } from "react-hook-form";
import useTrans from "../../hooks/translation";
import { SCREEN_TITLE } from '../../styles/general';
import { ActivityFiltersModal } from "../../components/Activity/Filters/ActivityFiltersModal";
import { FiltersIcon } from "../../components/Svg/FiltersIcon";

const ListActivityScreen = () => {
    const [initialUserRegion, setInitialUserRegion] = useState<Region>();
    const isFocused = useIsFocused();
    const { t } = useTrans();

    const [modalOpen, setModalOpen] = useState(false);

    const {
        control,
        getValues,
    } = useForm();

    useEffect(() => {
        if (!isFocused) return;

        if (!initialUserRegion) {
            GetLocation.getCurrentPosition({
                enableHighAccuracy: true,
                timeout: 20000,
            }).then((location: Location) => {
                setInitialUserRegion({
                    latitude: location.latitude,
                    longitude: location.longitude,
                    latitudeDelta: 0.8,
                    longitudeDelta: 0.8,
                })
            })
                .catch(error => {
                    const { code, message } = error;
                    console.warn(code, message);
                })
        }
    }, [isFocused]);

    return (
        <View style={{ flex: 1, position: 'relative' }}>
            <View style={styles.titleWithFiltersContainer}>
                <Text style={SCREEN_TITLE}>Activités</Text>
                <TouchableOpacity onPress={() => setModalOpen(true)}>
                    <View style={styles.filtersButtonContainer}>
                        <FiltersIcon color={"white"} width={12} height={8} />
                        <Text style={{ color: 'white', marginLeft: 10, fontSize: 12 }}>{t('Filters')}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <ActivityFiltersModal open={modalOpen} control={control} closeModal={() => setModalOpen(false)} handleSubmit={() => setModalOpen(false)} getValues={getValues} />
            <MapView style={{ flex: 1 }}
                     initialRegion={initialUserRegion}
                     provider={PROVIDER_GOOGLE}
            >
            </MapView>
            <View style={{
                position: "absolute",
                bottom: 40,
            }}>
            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    titleWithFiltersContainer: {
        position: 'absolute',
        zIndex: 10,
        top: '5%',
        paddingHorizontal: 30,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    filtersButtonContainer: {
        width: 95,
        height: 30,
        paddingHorizontal: 10,
        paddingVertical: 7,
        backgroundColor: COLORS.primary,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 50,
        elevation: 1
    }
});

export default ListActivityScreen;
