import React, {useEffect, useState} from "react";
import {View, StyleSheet, TextInput, FlatList, RefreshControl} from "react-native";

import Activity from "../../models/Activity";
import ActivityComponent from "../../components/Activity/ActivityComponent";
import {useDispatch, useSelector} from "react-redux";
import { Actions } from "../../store/ducks/search-activities";
import GetLocation, {Location} from "react-native-get-location";

const ListActivityScreen = () => {

    const search = useSelector(state => state.searchActivity);
    const dispatch = useDispatch();

    const [refreshing, setRefreshing] = useState<boolean>(false);

    const [inputText, setInputText] = useState('');

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => setRefreshing(false), 2000);
    }

    useEffect(() => {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 20000,
        }).then((location: Location) => {
            dispatch(Actions.requestSearchActivities({
                latitude: location.latitude,
                longitude: location.longitude
            }, 50))
        })
        .catch(error => {
            const { code, message } = error;
            console.warn(code, message);
        })
    }, [])

    return (
        <View style={styles.container}>
            <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}>
                <TextInput
                    style={styles.textInput}
                    placeholder={"Rechercher"}
                    value={inputText}
                    onChangeText={setInputText}
                />
                <FlatList
                    data={search.data}
                    renderItem={({ item }: {item: Activity}) => (
                        <View style={styles.row}>
                            <ActivityComponent activity={item}/>
                        </View>
                    )}
                />
            </RefreshControl>
        </View>
    );
}

const styles = StyleSheet.create({
    textInput: {
        fontSize: 20,
        marginBottom: 20,
    },
    container: {
        margin: 20,
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderColor: "lightgrey",
    },
    iconContainer: {
        backgroundColor: '#e7e7e7',
        padding: 7,
        borderRadius: 10,
        marginRight: 15,
    },
    descText: {

    },
});

export default ListActivityScreen;
