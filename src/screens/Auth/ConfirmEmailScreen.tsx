import React, { useRef } from "react";
import {View, Text, TouchableOpacity, SafeAreaView } from "react-native";
import { StyleType, CustomButton } from "../../components/Form/CustomButton/CustomButton";
import { CommonActions, useNavigation } from "@react-navigation/native";
import { Icon } from "react-native-elements";
import { BOTTOM } from "../../styles/general";
import { useForm } from "react-hook-form";
import FormInput from "../../components/Form/FormInput/FormInput";
import api from '../../services/api';
import {ApiResponseModal} from "../../components/Modal/ApiResponseModal";
import { ROOT_VIEW, TITLE } from "../../styles/general";

export const ConfirmEmailScreen = ({ route }: any) => {

    const modalCheckCode = useRef();
    const modalResendEmail = useRef();

    const navigation = useNavigation();

    const email = route.params?.email ?? '';
    console.log('email', email);

    const { handleSubmit, control, getValues, setError } = useForm({
        defaultValues: {
            email,
        }
    });

    const onConfirmPressed = (data) => {
        modalCheckCode.current?.open();

        api.post('auth/check-code', data).then(data => {
            if(data.success) {
                modalCheckCode.current?.update(false, true, 'Votre compte est activé');
            }
        }).catch(err => {
            setError('code', {
                type: 'custom',
                message: err.response.data.error
            });
            modalCheckCode.current?.update(false, false, 'Mauvais code');
        })
    }

    const goBackToLogin = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'SignIn',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                },
            })
        );
    }

    const onResendPressed = () => {
        modalResendEmail.current?.open();
        api.post('auth/resend-code', getValues()).then(data => {
            if(data.success) {
                modalResendEmail.current?.update(false, true, 'Un email vous a été envoyé');
            }
        }).catch(err => {
            setError('code', {
                type: 'custom',
                message: err.response.data.error
            });
            modalResendEmail.current?.update(false, false, err.response.data.error);
        })
    }

    return (
        <SafeAreaView style={ ROOT_VIEW }>

            <ApiResponseModal loadingTitle={"Vérification du code"} successCallback={goBackToLogin} ref={modalCheckCode} />
            <ApiResponseModal loadingTitle={"Renvoi d'un code"} ref={modalResendEmail} />

            <View style={{ width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <TouchableOpacity onPress={goBackToLogin}>
                    <Icon name={"chevron-left"}></Icon>
                </TouchableOpacity>
                <Text style={ TITLE }>Confirmez votre compte</Text>
            </View>

            <View style={{ marginTop: 20, width: '100%' }}>
                <FormInput
                    name={"email"}
                    control={control}
                    placeholder="Email"
                    rules={{
                        required: true,
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "Adresse email invalide"
                        }
                    }}
                />
            </View>

            <View style={{ marginTop: 20, width: '100%' }}>
                <FormInput
                    name={"code"}
                    control={control}
                    maxLength={5}
                    placeholder="Code de confirmation"
                    rules={{
                        required: true,
                        minLength: 5,
                        maxLength: 5
                    }}
                />
            </View>

            <View style={BOTTOM}>
                <CustomButton
                    text="Confirmer"
                    onPress={handleSubmit(onConfirmPressed)}
                    type={StyleType.PRIMARY}
                />

                <CustomButton
                    text="Re-envoyer le code"
                    onPress={handleSubmit(onResendPressed)}
                    type={StyleType.SECONDARY}
                />
            </View>
        </SafeAreaView>
    )
}

export default ConfirmEmailScreen;
