import React, {useRef, useState} from "react";
import {View, Text, StyleSheet, ScrollView, SafeAreaView, TouchableOpacity} from "react-native";
import { StyleType, CustomButton } from "../../components/Form/CustomButton/CustomButton";
import {CommonActions, useNavigation} from "@react-navigation/native";
import {ApiResponseModal} from "../../components/Modal/ApiResponseModal";
import {Icon} from "react-native-elements";
import FormInput from "../../components/Form/FormInput/FormInput";
import {BOTTOM} from "../../styles/general";
import {useForm} from "react-hook-form";
import api from "../../services/api";
import { ROOT_VIEW, TITLE } from '../../styles/general';

export const ForgotPasswordScreen = () => {
    const navigation = useNavigation();

    const modal = useRef();

    const { control, handleSubmit } = useForm();

    const onSuccess = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'NewPassword',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                },
            })
        );
    }

    const onConfirmPressed = (formData) => {
        modal.current?.open();

        api.post('auth/new-password', formData).then(data => {
            modal.current?.update(false, true, 'Un email vous a été envoyé', [
                'Renseignez le code pour changer votre mot de passe'
            ]);
        }).catch(err => {
            modal.current?.update(false, false, 'Une erreur est survenue');
        });
    }

    const goBackToLogin = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'SignIn',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                },
            })
        );
    }

    return (
        <SafeAreaView style={ROOT_VIEW}>

            <ApiResponseModal loadingTitle={"Envoi d'un email"} successCallback={onSuccess} ref={modal} />

            <View style={{ width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <TouchableOpacity onPress={goBackToLogin}>
                    <Icon name={"chevron-left"}></Icon>
                </TouchableOpacity>
                <Text style={TITLE}>Changer votre mot de passe</Text>
            </View>

            <Text>Vous recevrez un email contenant un code qui vous permettra de changer votre mot de passe.</Text>

            <View style={{ marginTop: 20, width: '100%' }}>
                <FormInput
                    name={"email"}
                    control={control}
                    placeholder="Email"
                    rules={{
                        required: true,
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                            message: "Adresse email invalide"
                        }
                    }}
                />
            </View>

            <View style={BOTTOM}>
                <CustomButton
                    text="Confirmer"
                    onPress={handleSubmit(onConfirmPressed)}
                    type={StyleType.PRIMARY}
                />
            </View>
        </SafeAreaView>
    )
}

export default ForgotPasswordScreen;
