import React, {useRef, useState} from "react";
import {View, Text, StyleSheet, ScrollView, SafeAreaView, TouchableOpacity} from "react-native";
import { StyleType, CustomButton } from "../../components/Form/CustomButton/CustomButton";
import {CommonActions, useNavigation} from "@react-navigation/native";
import {ApiResponseModal} from "../../components/Modal/ApiResponseModal";
import {Icon} from "react-native-elements";
import FormInput from "../../components/Form/FormInput/FormInput";
import {BOTTOM, FORM_GROUP} from "../../styles/general";
import {useForm, useWatch} from "react-hook-form";
import api from "../../services/api";

export const NewPasswordScreen = () => {
    const modal = useRef();

    const { control, handleSubmit } = useForm();

    const navigation = useNavigation();

    const password = useWatch({
        control,
        name: "plainPassword",
    });

    const onConfirmPressed = (formData) => {
        delete formData.repeatPassword;

        modal.current?.open();

        api.post('auth/update-password', formData).then(data => {
            modal.current?.update(false, true, 'Un email vous a été envoyé', [
                'Renseignez le code pour changer votre mot de passe'
            ]);
        }).catch(err => {
            console.log(err.response.data);
            modal.current?.update(false, false, 'Une erreur est survenue');
        })

        return;
    }

    const goBackToLogin = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'SignIn',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                },
            })
        );
    }

    return (
        <SafeAreaView style={styles.root}>

            <ApiResponseModal loadingTitle={"Mise à jour du mot de passe"} successCallback={goBackToLogin} ref={modal} />

            <View style={{ width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <TouchableOpacity onPress={goBackToLogin}>
                    <Icon name={"chevron-left"}></Icon>
                </TouchableOpacity>
                <Text style={styles.title}>Changer votre mot de passe</Text>
            </View>

            <View style={{ marginTop: 20, width: '100%' }}>
                <FormInput
                    name={"code"}
                    control={control}
                    maxLength={5}
                    placeholder="Code de confirmation"
                    rules={{
                        required: true,
                        minLength: 5,
                        maxLength: 5
                    }}
                />
            </View>

            <View style={FORM_GROUP}>
                <FormInput
                    name={"plainPassword"}
                    control={control}
                    label={'Mot de passe'}
                    placeholder="Mot de passe"
                    secureTextEntry
                    rules={{
                        validate: (value: string) => {
                            if(value.length < 8) return 'Le mot de passe doit contenir au moins 8 lettres'
                            return true;
                        }
                    }}
                />
            </View>

            <View style={FORM_GROUP}>
                <FormInput
                    name={"repeatPassword"}
                    control={control}
                    label={'Répétez votre mot de passe'}
                    placeholder="Répétez votre mot de passe"
                    secureTextEntry
                    rules={{
                        validate: (value: string) => value === password || "Les mot de passes ne sont pas les mêmes."
                    }}
                />
            </View>

            <View style={BOTTOM}>
                <CustomButton
                    text="Confirmer"
                    onPress={handleSubmit(onConfirmPressed)}
                    type={StyleType.PRIMARY}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    root: {
        height: '100%',
        position: 'relative',
        alignItems: 'center',
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#051C60',
        margin: 10,
    },
    text: {
        color: 'grey',
        marginVertical: 10,
    },
    link: {
        color: '#FDB075',
    }
});

export default NewPasswordScreen;
