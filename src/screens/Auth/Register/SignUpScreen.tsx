import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  BackHandler
} from 'react-native';
import { CommonActions, useNavigation } from '@react-navigation/native';
import COLORS from '../../../theme/layout/colors';
import SignUpBase from '../../../components/Auth/Register/SignUpBase';
import SignUpInfos from '../../../components/Auth/Register/SignUpInfos';
import SignUpSports from '../../../components/Auth/Register/SignUpSports';
import SignUpParameters from '../../../components/Auth/Register/SignUpParameters';
import { useForm } from 'react-hook-form';
import api from '../../../services/api';
import { ApiResponseModal } from "../../../components/Modal/ApiResponseModal";
import useTrans from "../../../hooks/translation";

export const SignUpScreen = () => {
  const navigation = useNavigation();

  const { t } = useTrans();

  const {
    setValue,
    handleSubmit,
    control,
    getValues,
    setError,
  } = useForm();

  const modal = useRef();

  const [stepIndex, setStepIndex] = useState<number>(1);

  useEffect(() => {
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => handleBackPress());

    return () => backHandler.remove()
  }, []);

  const [query, setQuery] = useState<any>({
    visible: false,
    loading: false,
    success: false
  });

  const handleBackPress = () => {
      console.log("step", stepIndex);
      if(stepIndex > 1) {
        console.log("preuviyssteo");
        previousStep();
      } else {
        console.log("navig go back");
        navigation.goBack();
      }

      return true;
  }

  const onSignInPressed = () => {
    navigation.dispatch(
      CommonActions.navigate({
        name: 'SignIn',
        params: {
          headerLeft: null,
          gestureEnabled: false,
        },
      }),
    );
  };

  const onTermsOfUsePressed = () => {
    console.warn('terms of use');
  };
  const onPrivacyPressed = () => {
    console.warn('privacy');
  };

  const nextStep = () => {
    if (steps.length > stepIndex) {
      setStepIndex(stepIndex + 1);
    }
  };

  const onRegister = (data: any) => {
    let format: any = data;
    format.latitude = format.location.lat;
    format.longitude = format.location.lng;
    delete format.location;
    delete format.repeatPassword;
    format.notificationRadius = format.notificationRadius[0];

    setQuery({ query, ...{ loading: true, visible: true }})

    console.log(format);

    modal.current?.open();

    api
      .post('create-account', format)
      .then(data => {
        console.log('data', data.data);
        modal.current?.update(false, true, "Votre compte a bien été créé", ['Vérifiez maintenant votre boite mail!']);

       /* setQuery({ query, ...{
          loading: false,
          success: true,
          messageTitle: 'Votre compte a bien été créé',
          messagesDescription: ['Vérifiez maintenant votre boite mail!']
        }})*/
      })
      .catch(err => {
        console.log('error', err.response.data.errors);
        let notHandledErrors = [];
        if(err.response.data.errors !== undefined && typeof err.response.data.errors === 'object') {
          for(const [field, value] of Object.entries(err.response.data.errors)) {
            if(getValues(field) !== undefined) {
              setError(field, {
                type: "custom",
                message: value
              });
            } else {
              notHandledErrors.push(field + ": " + value);
            }
          }
        }

        modal.current?.update(false, false, "Une erreur est survenue", notHandledErrors);
        /*setQuery({ query, ...{ loading: false, success: false, messageTitle: "", messagesDescription: notHandledErrors }})*/
        setStepIndex(1);
      });
  };

  const closeModal = () => {
    /*setQuery({ visible: false });*/
      navigation.dispatch(
          CommonActions.navigate({
            name: 'ConfirmEmail',
            params: {
              email: getValues('email'),
              headerLeft: null,
              gestureEnabled: false,
            },
          })
      );
  }

  const previousStep = () => {
    if (stepIndex > 1) {
      setStepIndex(stepIndex - 1);
    }
  };

  const steps = [
    {
      title: t('Sign Up'),
      infoText: true,
      component: () => (
        <SignUpBase
          handleSubmit={handleSubmit}
          setValue={setValue}
          control={control}
          nextStep={nextStep}
        />
      ),
    },
    {
      title: t('Your informations'),
      infoText: false,
      component: () => (
        <SignUpInfos
          handleSubmit={handleSubmit}
          control={control}
          nextStep={nextStep}
        />
      ),
    },
    {
      title: t('Your favorite sports'),
      infoText: false,
      component: () => (
        <SignUpSports
          handleSubmit={handleSubmit}
          control={control}
          nextStep={nextStep}
        />
      ),
    },
    {
      title: t('Your parameters'),
      infoText: false,
      component: () => (
        <SignUpParameters
          setValue={setValue}
          handleSubmit={handleSubmit}
          control={control}
          getValues={getValues}
          nextStep={handleSubmit(onRegister)}
        />
      ),
    },
  ];

  const step = steps[stepIndex - 1];

  return (
    <SafeAreaView style={styles.root}>
      <ApiResponseModal ref={modal} loadingTitle={"Inscription"} successCallback={closeModal}/>
     {/* <Modal
          visible={query.visible}
          animationType="fade"
          transparent={true}
      >
        <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%', backgroundColor: 'rgba(0, 0, 0, 0.6)' }}>
          <View style={{ backgroundColor: 'white', width: '90%', height: '50%', borderRadius: 30, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            {query.loading && <View style={{ padding: 20 }}>
              <View>
                <ActivityIndicator size="large" color={COLORS.primary} />
                <Text style={{ textAlign: 'center', fontSize: 20, marginTop: 20 }}>Inscription</Text>
              </View>
            </View>}
            {!query.loading && <View style={{ padding: 20}}>
              <View>
                <Icon type="material" name={query.success ? "check" : "error-outline"} size={60} color={query.success ? COLORS.primary : COLORS.danger }/>
                <Text style={{ textAlign: 'center', fontSize: 18, color: query.success ? COLORS.primary : COLORS.danger }}>{query.messageTitle}</Text>

                <View style={{ marginTop: 20 }}>
                  {query.messagesDescription && query.messagesDescription.map((msg: string, index: number) => <Text key={index}>{msg}</Text>)}
                </View>
              </View>
              <View style={{ marginTop: 20 }}>
                <CustomButton
                    onPress={closeModal}
                    text={"C'est compris"} />
              </View>
            </View>}
          </View>
        </View>
      </Modal>*/}

      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 14,
          justifyContent: 'space-between',
        }}>
        <Text style={styles.title}>{step.title}</Text>
        <Text style={{color: COLORS.primary}}>Étape {stepIndex}/3</Text>
      </View>

      {step.component()}

      {step.infoText && (
        <View>
          <Text style={styles.text}>
            En vous inscrivant, vous confirmez que vous acceptez nos{' '}
            <Text style={styles.link} onPress={onTermsOfUsePressed}>
              contrats d'utilisation
            </Text>{' '}
            et notre{' '}
            <Text style={styles.link} onPress={onPrivacyPressed}>
              politique de confidentialité
            </Text>
            .
          </Text>

          <TouchableOpacity
            onPress={onSignInPressed}
            style={{display: 'flex', flexDirection: 'row'}}>
            <Text>{t('Already have an account ?')}</Text>
            <Text style={{fontWeight: 'bold'}}> {t('Sign In')}</Text>
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    height: '100%',
    backgroundColor: 'white',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#080808',
  },
  text: {
    color: 'grey',
    marginVertical: 10,
  },
  text_error: {
    color: '#dd494e',
    padding: 2,
    margin: 2,
  },
  text_success: {
    backgroundColor: '#36D297',
    color: '#18A774',
    padding: 2,
    margin: 2,
  },
  link: {
    color: '#FDB075',
  },
});

export default SignUpScreen;
