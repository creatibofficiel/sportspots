import React, {useEffect, useState} from "react";
import {
    View,
    Text,
    Image,
    StyleSheet,
    useWindowDimensions,
    ScrollView,
    TouchableOpacity,
} from "react-native";
import FormInput from "../../components/Form/FormInput/FormInput";
import { StyleType, CustomButton } from "../../components/Form/CustomButton/CustomButton";
import { CommonActions, useNavigation } from "@react-navigation/native";
import { Actions } from "../../store/ducks/auth";
import { useDispatch, useSelector } from "react-redux";
import { FORM_GROUP } from '../../styles/general';
import { useForm } from "react-hook-form";
import useTrans from "../../hooks/translation";
import {EyeIcon} from "../../components/Svg/EyeIcon";
import COLORS from "../../theme/layout/colors";

export const SignInScreen = () => {
    const { handleSubmit, control } = useForm();

    const errorDetail = useSelector(state => state.auth.errorDetail);

    const dispatch = useDispatch();

    const { height } = useWindowDimensions();
    const navigation = useNavigation();
    const {t} = useTrans();
    const [ passwordVisible, setPasswordVisible ] = useState<boolean>(false);

    const [errorMessage, setError] = useState("");

    useEffect(() => {
        if(errorDetail !== undefined && errorDetail.message !== null) setError(errorDetail.message);
    }, [errorMessage]);

    const onSignInPressed = (data: any) => dispatch(Actions.requestLoginRequest(data));

    const onForgotPasswordPressed = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'ForgotPassword',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                },
            })
        );
    }

    const onSignUpPressed = () => {
        navigation.dispatch(
            CommonActions.navigate({
                name: 'SignUp',
                params: {
                    headerLeft: null,
                    gestureEnabled: false,
                },
            })
        );
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#fff' }}>
            <View style={styles.root}>
                <Image source={require('../../../assets/images/logo.png')}
                    style={[styles.logo, { height: height * 0.3 }]}
                    resizeMode="contain"
                />

                {errorMessage.length > 0 && <Text style={styles.text_error}>{errorMessage}</Text>}

                <View style={FORM_GROUP}>
                    <FormInput
                        control={control}
                        name={"username"}
                        placeholder="Email"
                        rules={{
                            required: true,
                            pattern: {
                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                                message: "Adresse email invalide"
                            }
                        }}
                    />
                </View>

                <View style={FORM_GROUP}>
                    <View style={{position: 'relative'}}>
                        <FormInput
                            control={control}
                            name={"password"}
                            placeholder={t('Password')}
                            secureTextEntry={!passwordVisible}
                            rules={{
                                required: true,
                            }}
                        />
                        <TouchableOpacity activeOpacity={.5} onPress={() => setPasswordVisible(!passwordVisible)} style={{position: 'absolute', right: 15, height: '100%', justifyContent: 'center'}}>
                            <EyeIcon color={COLORS.primary} width={20} height={18} visible={passwordVisible} />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity activeOpacity={.4} onPress={onForgotPasswordPressed} style={{ marginLeft: 'auto', marginTop: 12 }}>
                        <Text>{t('Forgot password')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={FORM_GROUP}>
                    <CustomButton
                        text={t('Log in')}
                        onPress={handleSubmit(onSignInPressed)}
                        type={StyleType.PRIMARY}
                    />

                    <CustomButton
                        text={t('No account ? Create one')}
                        onPress={onSignUpPressed}
                        type={StyleType.PRIMARY}
                    />
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        padding: 20,
    },
    logo: {
        width: '70%',
        maxWidth: 300,
        maxHeight: 200,
    },
    text_error: {
        backgroundColor: '#FB7073',
        color: '#D7484B',
        padding: 2,
        margin: 2,
    },
    text_success: {
        backgroundColor: '#36D297',
        color: '#18A774',
        padding: 2,
        margin: 2,
    },
})

export default SignInScreen;
