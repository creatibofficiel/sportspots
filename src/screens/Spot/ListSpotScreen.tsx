import React, { createRef, useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MapView, { Coordinate, PROVIDER_GOOGLE, Region } from 'react-native-maps';
import GetLocation, { Location } from 'react-native-get-location'
import CustomMarker from "../../components/Map/CustomMarker";
import SpotCard from "../../components/Spot/SpotCard";
import Spot from '../../models/Spot';
import { useIsFocused } from "@react-navigation/core";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../../store/ducks/search-spot";
import COLORS from "../../theme/layout/colors";
import useTrans from "../../hooks/translation";
import { FiltersIcon } from "../../components/Svg/FiltersIcon";
import { useForm } from "react-hook-form";
import { SpotFiltersModal } from "../../components/Spot/Filters/SpotFiltersModal";
import { SCREEN_TITLE } from "../../styles/general";

const ListSpotScreen = () => {
    const search = useSelector(state => state.searchSpot);

    const [modalOpen, setModalOpen] = useState(false);
    const {
        handleSubmit,
        control,
        getValues,
    } = useForm();

    const [selectedSpot, setSelectedSpot] = useState<Spot|null>();
    const [initialUserRegion, setInitialUserRegion] = useState<Region>();
    const map: React.RefObject<MapView> = createRef();
    const isFocused = useIsFocused();

    const dispatch = useDispatch();
    const { t } = useTrans();

    const fetchSpots = (data: any) => {
        setSelectedSpot(null);
        dispatch(Actions.requestSearchSpots(data));
    };

    useEffect(() => {
       fetchSpots({
           lat: initialUserRegion?.latitude ?? 0,
           lng: initialUserRegion?.longitude ?? 0,
           radius: 50
       });
    }, [initialUserRegion]);

    const searchSpots = (data: any) => {
        data.latitude = data.location.lat;
        data.longitude = data.location.lng;
        data.radius = data.radius[0];
        delete data.location;
        setModalOpen(false);

        fetchSpots(data);
    };

    useEffect(() => {
        if (!isFocused) return;

        if (!initialUserRegion) {
            GetLocation.getCurrentPosition({
                enableHighAccuracy: true,
                timeout: 20000,
            }).then((location: Location) => {
                setInitialUserRegion({
                    latitude: location.latitude,
                    longitude: location.longitude,
                    latitudeDelta: 0.8,
                    longitudeDelta: 0.8,
                })
            })
            .catch(error => {
                const { code, message } = error;
                console.warn(code, message);
            })
        }
    }, [isFocused]);

    return (
        <View style={{ flex: 1, position: 'relative' }}>
            <SpotFiltersModal open={modalOpen} closeModal={() => setModalOpen(false)} control={control} handleSubmit={handleSubmit(searchSpots)} getValues={getValues}/>
            <View style={styles.titleWithFiltersContainer}>
                <Text style={SCREEN_TITLE}>Spots</Text>
                <TouchableOpacity onPress={() => setModalOpen(true)}>
                    <View style={styles.filtersButtonContainer}>
                        <FiltersIcon color={"white"} width={12} height={8}/>
                        <Text style={{ color: 'white', marginLeft: 10, fontSize: 12 }}>{t('Filters').toUpperCase()}</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <MapView style={{ flex: 1 }}
                initialRegion={initialUserRegion}
                 onPanDrag={(ev) => {
                     setSelectedSpot(null)
                 }}
                ref={map}
                provider={PROVIDER_GOOGLE}
            >
                {search.data.length > 0 && Object.values(search.data).map((spot: any) => {
                    return (
                        <CustomMarker
                            item={spot}
                            isSelected={spot.id === selectedSpot?.id}
                            onPress={() => {
                                setSelectedSpot(spot);
                            }}
                            key={spot.id}
                        />
                    )
                })}
            </MapView>
            {selectedSpot && <View style={{
                position: "absolute",
                width: '100%',
                paddingHorizontal: 30,
                bottom: 115,
            }}>
                <SpotCard spot={selectedSpot} key={selectedSpot.id} />
            </View>}
        </View >
    );
}

const styles = StyleSheet.create({
    titleWithFiltersContainer: {
        position: 'absolute',
        zIndex: 10,
        top: '5%',
        paddingHorizontal: 30,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    filtersButtonContainer: {
        width: 95,
        height: 30,
        paddingHorizontal: 10,
        paddingVertical: 7,
        backgroundColor: COLORS.primary,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 50,
        elevation: 1
    }
});

export default ListSpotScreen;
