import axios from 'axios';
import store from '../store/index';
import { Actions } from "../store/ducks/auth";
import {call} from "redux-saga/effects";

const api = axios.create({
    baseURL: 'https://sportspots.fr/api/',
    headers: {
        'content-type': 'application/json',
        "Access-Control-Allow-Origin": "*"
    },
});

api.interceptors.response.use((response) => {
    return response
}, async function (error) {
    const originalRequest = error.config;

    console.log(error.response.status === 401 && error.response.data.message);
    if(error.response.status === 401 && error.response.data.message === "Expired JWT Token") {
        console.log(originalRequest._retry);
        if(!originalRequest._retry) {
            originalRequest._retry = true;
            api.post(`token/refresh`, { refresh_token: store.getState().auth.data.refresh_token }).then(data => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.data.token
                console.log("Good, retry");
                store.dispatch(Actions.requestLoginSuccess(data.data));
                console.log(originalRequest);
                return api(originalRequest);
            }).catch(err => {
                store.dispatch(Actions.requestLogout());
            });
        }
    }
    return Promise.reject(error);
});

export default api;
