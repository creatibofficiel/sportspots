export interface GeocodeResultItem {
    location: { lat: number; lng: number };
    address: string;
}

export interface Geocodable {
    getResults(search: string): Promise<GeocodeResultItem[]>;
}
