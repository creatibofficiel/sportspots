import { Geocodable, GeocodeResultItem } from "./Geocodable";
import { MAP_BOX_TOKEN } from "react-native-dotenv";

const API_URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/";

export class MapBoxGeocodable implements Geocodable {
    getResults(search: string): Promise<GeocodeResultItem[]> {
        console.log(API_URL + `${search}.json?access_token=${MAP_BOX_TOKEN}&limit=10`);
        return new Promise((resolve, reject) => {
            fetch(API_URL + `${search}.json?access_token=${MAP_BOX_TOKEN}&limit=10`).then(data => data.json()).then(data => {
                let geoItems: GeocodeResultItem[] = [];

                data.features.forEach((e: any) => {
                    geoItems.push({
                        address: e.place_name,
                        location: {
                            lat: e.geometry.coordinates[0],
                            lng: e.geometry.coordinates[1]
                        }
                    });
                })

                console.log(geoItems);

                resolve(geoItems);
            }).catch(error => {
                console.log("got error ", error);
                reject(error);
            })
        })
    }
}
