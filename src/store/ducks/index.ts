import { combineReducers } from 'redux';

import user from './user';
import spot from './spot';
import spotComments from './spot-comments';
import searchSpot from './search-spot';
import searchActivity from './search-activities';
import auth from './auth';
import searchSport from './search-sports';

export interface StateType {
    auth: any;
    searchSpot: any;
    searchActivity: any;
    searchSport: any;
    user: any;
    spot: any;
}

export default combineReducers({
    auth,
    searchSpot,
    searchActivity,
    searchSport,
    user,
    spot,
    spotComments
});
