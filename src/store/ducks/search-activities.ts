export const Types = {
    SEARCH_ACTIVITIES_REQUEST: 'activity/SEARCH_ACTIVITY_REQUEST',
    SEARCH_ACTIVITIES_REQUEST_SUCCESS: 'activity/SEARCH_ACTIVITY_REQUEST_SUCCESS',
    SEARCH_ACTIVITIES_REQUEST_FAILURE: 'activity/SEARCH_ACTIVITY_REQUEST_FAILURE',
    RESET_DATA: 'activity/RESET_DATA',
};

const INITIAL_STATE = {
    loading: false,
    error: false,
    data: [],
};

export const Actions = {
    requestSearchActivities: (location: any, radius: number, activities: string[] = []) => ({
        type: Types.SEARCH_ACTIVITIES_REQUEST,
        payload: { location, activities, radius },
    }),

    requestSearchActivitiesSuccess: (data: []) => ({
        type: Types.SEARCH_ACTIVITIES_REQUEST_SUCCESS,
        payload: [ ...data ],
    }),

    requestSearchActivitiesFailure: () => ({
        type: Types.SEARCH_ACTIVITIES_REQUEST_FAILURE,
    }),

    resetState: () => ({
        type: Types.RESET_DATA,
    }),
};

const searchActivities = (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case Types.SEARCH_ACTIVITIES_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case Types.SEARCH_ACTIVITIES_REQUEST_SUCCESS:
            return {
                ...state,
                data: action.payload,
                loading: false,
                error: false,
            };

        case Types.SEARCH_ACTIVITIES_REQUEST_FAILURE:
            return {
                ...state,
                loading: false,
                error: true,
            };

        case Types.RESET_DATA:
            return INITIAL_STATE;

        default:
            return state;
    }
};

export default searchActivities;
