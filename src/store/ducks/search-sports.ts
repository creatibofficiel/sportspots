export const Types = {
    SEARCH_SPORTS_REQUEST: 'sport/SEARCH_SPORT_REQUEST',
    SEARCH_SPORTS_REQUEST_SUCCESS: 'sport/SEARCH_SPORT_REQUEST_SUCCESS',
    SEARCH_SPORTS_REQUEST_FAILURE: 'sport/SEARCH_SPORT_REQUEST_FAILURE',
    RESET_DATA: 'sport/RESET_DATA',
};

const INITIAL_STATE = {
    loading: false,
    error: false,
    data: [],
};

export const Actions = {
    requestSearchSports: (search: string|null = null) => ({
        type: Types.SEARCH_SPORTS_REQUEST,
        payload: { search },
    }),

    requestSearchSportsSuccess: (data: []) => ({
        type: Types.SEARCH_SPORTS_REQUEST_SUCCESS,
        payload: [ ...data ],
    }),

    requestSearchSportsFailure: () => ({
        type: Types.SEARCH_SPORTS_REQUEST_FAILURE,
    }),

    resetState: () => ({
        type: Types.RESET_DATA,
    }),
};

const spot = (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case Types.SEARCH_SPORTS_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case Types.SEARCH_SPORTS_REQUEST_SUCCESS:
            return {
                ...state,
                data: action.payload,
                loading: false,
                error: false,
            };

        case Types.SEARCH_SPORTS_REQUEST_FAILURE:
            return {
                ...state,
                loading: false,
                error: true,
            };

        case Types.RESET_DATA:
            return INITIAL_STATE;

        default:
            return state;
    }
};

export default spot;
