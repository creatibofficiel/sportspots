export const Types = {
    GET_COMMENTS_REQUEST: 'spot/GET_COMMENTS_REQUEST',
    GET_COMMENTS_REQUEST_SUCCESS: 'spot/GET_COMMENTS_REQUEST_SUCCESS',
    GET_COMMENTS_REQUEST_FAILURE: 'spot/GET_COMMENTS_REQUEST_FAILURE',
    RESET_DATA: 'spot/RESET_DATA',
};

const INITIAL_STATE = {
    loading: false,
    error: false,
    data: {},
};

export const Actions = {
    requestSpotCommentsRequest: (id: string, page: number) => ({
        type: Types.GET_COMMENTS_REQUEST,
        payload: { id, page },
    }),

    requestSpotCommentsSuccess: (data: any) => ({
        type: Types.GET_COMMENTS_REQUEST_SUCCESS,
        payload: { ...data },
    }),

    requestSpotCommentsFailure: () => ({
        type: Types.GET_COMMENTS_REQUEST_FAILURE,
    }),

    resetState: () => ({
        type: Types.RESET_DATA,
    }),
};

const spotComments = (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case Types.GET_COMMENTS_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case Types.GET_COMMENTS_REQUEST_SUCCESS:
            return {
                ...state,
                data: action.payload,
                loading: false,
                error: false,
            };

        case Types.GET_COMMENTS_REQUEST_FAILURE:
            return {
                ...state,
                loading: false,
                error: true,
            };

        case Types.RESET_DATA:
            return INITIAL_STATE;

        default:
            return state;
    }
};

export default spotComments;
