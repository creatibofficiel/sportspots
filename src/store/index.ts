import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';

import sagas from './sagas';
import reducers from './ducks';
import api from "../services/api";

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(persistedReducer, applyMiddleware(...middleware));

sagaMiddleware.run(sagas);

export default store;
export const persistor = persistStore(store, null, () => {
    if(store.getState().auth.data?.token !== null) {
        api.defaults.headers.common = {'Authorization': `Bearer ${store.getState().auth.data?.token}`}
    }
});
