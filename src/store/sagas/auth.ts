import { call, put } from 'redux-saga/effects';

import { Actions as LoginActions } from '../ducks/auth';
import api from '../../services/api';

export function* login(action: any) {
    try {
        console.log(action.payload);
        const login = yield call(api.post, `login_check`, action.payload);

        api.defaults.headers.common = {'Authorization': `Bearer ${login.data.token}`}
        yield put(LoginActions.requestLoginSuccess(login.data));
    } catch (e) {
        console.log("Catch", e.response.data.message);
        yield put(LoginActions.requestLoginFailure(e.response.data));
    }
}

export function* refreshToken(action: any) {
    try {
        const login = yield call(api.post, `token/refresh`, {refresh_token: action.payload});

        console.log("On est au refreshToken retour", login.data.token);
        api.defaults.headers.common = {'Authorization': `Bearer ${login.data.token}`}
        yield put(LoginActions.requestLoginSuccess(login.data));
    } catch (e) {
        console.log("Catch", e.response.data.message);
        yield put(LoginActions.requestLoginFailure(e.response.data));
    }
}

export function* logout(action: any) {
    try {
        api.defaults.headers.common = {};
        yield call(api.post, `logout`);
    } catch (e) {}
}
