import { all, takeLatest } from "redux-saga/effects";

import { Types as AuthTypes } from '../ducks/auth';
import { Types as UserTypes } from '../ducks/user';
import { Types as SpotTypes } from '../ducks/spot';
import { Types as SearchSpotTypes } from '../ducks/search-spot';
import { Types as SearchActivityTypes } from '../ducks/search-activities';
import { Types as SearchSportTypes } from '../ducks/search-sports';
import { Types as SpotCommentTypes } from '../ducks/spot-comments';

import { fetchUser } from './user';
import { requestSearchSpots } from './search-spots';
import { requestSearchActivities } from './search-activities';
import { requestSearchSports } from './search-sports';
import { fetchSpot } from './spot';
import { fetchSpotComments } from './spot-comments';
import {login, logout, refreshToken} from "./auth";

export default function* rootSaga() {
    return yield all([
        takeLatest(AuthTypes.LOGIN_REQUEST, login),
        takeLatest(AuthTypes.LOGIN_REFRESH_TOKEN_REQUEST, refreshToken),
        takeLatest(AuthTypes.LOGOUT, logout),
        takeLatest(UserTypes.GET_DETAIL_REQUEST, fetchUser),
        takeLatest(SpotCommentTypes.GET_COMMENTS_REQUEST, fetchSpotComments),
        takeLatest(SpotTypes.GET_DETAIL_REQUEST, fetchSpot),
        takeLatest(SearchSpotTypes.SEARCH_SPOTS_REQUEST, requestSearchSpots),
        takeLatest(SearchActivityTypes.SEARCH_ACTIVITIES_REQUEST, requestSearchActivities),
        takeLatest(SearchSportTypes.SEARCH_SPORTS_REQUEST, requestSearchSports),
    ]);
}
