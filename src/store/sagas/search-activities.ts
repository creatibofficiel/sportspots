import { call, put } from 'redux-saga/effects';

import api from '../../services/api';
import { Actions as SearchActivitiesActions } from '../ducks/search-activities';

export function* requestSearchActivities(action: any) {
    try {
        const { location, activities, radius } = action.payload;

        const headers = {
            lat: location.latitude,
            lng: location.longitude,
        };

        const paramsMerged = Object.assign(headers, { activities }, { radius });

        const response = yield call(api.get, 'activities', {
            params: paramsMerged,
        });

        yield put(SearchActivitiesActions.requestSearchActivitiesSuccess(response.data));
    } catch (err) {
        console.log("error ", err.message);
        yield put(SearchActivitiesActions.requestSearchActivitiesFailure());
    }
}
