import { call, put } from 'redux-saga/effects';

import api from '../../services/api';
import { Actions as SearchSportsActions } from '../ducks/search-sports';

export function* requestSearchSports(action: any) {
    try {
        const response = yield call(api.get, 'sports', {
            params: action.payload,
        });

        yield put(SearchSportsActions.requestSearchSportsSuccess(response.data));
    } catch (err) {
        console.log("error ", err.message);
        yield put(SearchSportsActions.requestSearchSportsFailure());
    }
}
