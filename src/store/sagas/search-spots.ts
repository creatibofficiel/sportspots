import { call, put } from 'redux-saga/effects';

import api from '../../services/api';
import { Actions as SearchSpotsActions } from '../ducks/search-spot';

export function* requestSearchSpots(action: any) {
    try {
        const params = action.payload;

        const response = yield call(api.get, 'spots', {
            params: params,
        });

        yield put(SearchSpotsActions.requestSearchSpotsSuccess(response.data));
    } catch (err) {
        console.log("error ", err.message);
        yield put(SearchSpotsActions.requestSearchSpotsFailure());
    }
}
