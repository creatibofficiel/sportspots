import { call, put } from 'redux-saga/effects';

import { Actions as SpotCommentActions } from '../ducks/spot-comments';
import api from '../../services/api';

export function* fetchSpotComments(action: any) {
    try {
        const { id, page } = action.payload;
        const response = yield call(api.get, `/spot/${id}/comments`, {
            params: { page }
        });
        yield put(SpotCommentActions.requestSpotCommentsSuccess(response.data));
    } catch (err) {
        yield put(SpotCommentActions.requestSpotCommentsFailure());
    }
}
