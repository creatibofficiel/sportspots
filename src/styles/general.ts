import {StyleProp, TextStyle, ViewStyle} from "react-native";

const FORM_GROUP: StyleProp<ViewStyle> = {
    marginTop: 16,
    width: '100%',
};

const ROOT_VIEW: StyleProp<ViewStyle> = {
    height: '100%',
    backgroundColor: 'white',
    position: 'relative',
    alignItems: 'center',
    padding: 20,
};

const TITLE: StyleProp<TextStyle> = {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'Montserrat',
    color: '#051C60',
    margin: 10,
};


const SCREEN_TITLE: StyleProp<TextStyle> = {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#000B22'
};

const BOTTOM: StyleProp<ViewStyle> = {
    position: 'absolute',
    width: '100%',
    bottom: 24,
}

export { FORM_GROUP, BOTTOM, TITLE, ROOT_VIEW, SCREEN_TITLE };
