import { StyleSheet } from "react-native";
import COLORS from "../layout/colors";

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    text: {
        fontSize: 15,
        marginBottom: 14,
    },
    input: {
        backgroundColor: 'white',
        width: '100%',

        borderColor: 'rgba(0, 0, 0, 0.7)',
        borderWidth: 1.5,
        borderRadius: 16,

        display: 'flex',
        justifyContent: 'center',
        height: 64,
        paddingHorizontal: 19
    },
    inputInvalid: {
        borderColor: COLORS.danger
    },
    inputFocus: {
        borderColor: COLORS.primary
    }
});

export default styles;
