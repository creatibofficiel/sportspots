import { StyleSheet } from "react-native";
import COLORS from "../layout/colors";

const styles: { [key: string]: any; } = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '100%',

        borderColor: '#e8e8e8',
        borderWidth: 1,
        borderRadius: 5,

        paddingVertical: 0,
        paddingHorizontal: 10,
        marginVertical: 5,
    },
});

export default styles;
