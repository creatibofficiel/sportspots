import { StyleSheet } from "react-native";
import COLORS from "../layout/colors";

const styles: { [key: string]: any; } = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    text: {
        fontWeight: 'bold',
        color: COLORS.primary,
        fontSize: 16,
    },
    selectedText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white'
    },
    selectedInput: {
        backgroundColor: COLORS.primary,
    },
    input: {
        borderColor: COLORS.primary,
        borderWidth: 1.5,
        borderRadius: 16,
        width: '32%',
        marginTop: 12,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 20,
    }
})

export default styles;
