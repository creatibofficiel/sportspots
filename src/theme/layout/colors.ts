const COLORS = {
    gradient: {
        100: "#0AFF54",
        200: "#00F549",
        300: "#00E043",
        400: "#00CC3D",
        500: "#00b936",
        600: "#00A331",
        700: "#008F2B",
        800: "#007A25",
        900: "#00661F"
    },
    primary: "#3BB54A",
    primary_dark: '#97CA4D',
    primary_light: '#BFF176',
    white: '#F2F6F8',
    blue: '#1580AD',
    danger: '#ff3333'
};

export default COLORS;
