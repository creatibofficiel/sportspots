let timer: any = null;

export default function debounce(timeout: number, callback: any): void {
    if(timer != null) clearTimeout(timer);
    console.log(" calling callback");
    timer = setTimeout(callback, timeout);
}
